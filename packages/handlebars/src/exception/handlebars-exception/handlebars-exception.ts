import * as Handlebars from 'handlebars';

/**
 * 模板异常
 *
 * @author chitanda
 * @date 2021-12-29 16:12:06
 * @export
 * @class HandlebarsException
 * @extends {Error}
 */
export class HandlebarsException extends Error {
  constructor(message: string, _options?: Handlebars.HelperOptions) {
    super(message);
  }
}
