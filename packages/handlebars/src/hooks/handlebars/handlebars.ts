import { TemplateEngineHooks } from '@ibizlab/template-core';
import { SyncHook } from 'tapable';
import { HandlebarsEngine } from '../../handlebars/handlebars-engine';

/**
 * handlebars 引擎钩子
 *
 * @author chitanda
 * @date 2021-12-24 14:12:55
 * @export
 * @class HandlebarsHooks
 */
export class HandlebarsHooks implements TemplateEngineHooks {
  /**
   * 初始化之后
   *
   * @author chitanda
   * @date 2021-12-24 14:12:10
   */
  init = new SyncHook<[HandlebarsEngine]>(['handlebarsEngine']);
}
