import {
  ModelData,
  TemplateEngine,
  TemplateEngineHooks,
  winToUnixPath,
} from '@ibizlab/template-core';
import * as Handlebars from 'handlebars';
import { installHelpers } from '../helpers';
import { HandlebarsHooks } from '../hooks';
import { HelperUtil } from '../utils';

/**
 * 生成器
 *
 * @author chitanda
 * @date 2021-12-21 14:12:41
 * @export
 * @class HandlebarsEngine
 */
export class HandlebarsEngine implements TemplateEngine {
  /**
   * 编译参数
   *
   * @author chitanda
   * @date 2022-01-07 11:01:29
   * @protected
   * @type {CompileOptions}
   */
  protected compileOpt: CompileOptions = { compat: true, noEscape: true };

  /**
   * Handlebars 运行时参数
   *
   * @author chitanda
   * @date 2021-12-29 16:12:46
   * @protected
   * @type {Handlebars.RuntimeOptions}
   */
  protected runtimeOpt: Handlebars.RuntimeOptions = {
    allowProtoPropertiesByDefault: true,
  };

  /**
   * 忽略编译的上下文标签规则
   *
   * @author chitanda
   * @date 2021-12-30 09:12:35
   * @protected
   */
  protected tagReg = /^[a-zA-Z0-9:_-]+$/;

  /**
   * 模板引擎钩子
   *
   * @author chitanda
   * @date 2021-12-29 16:12:18
   * @type {TemplateEngineHooks}
   */
  hooks: TemplateEngineHooks = new HandlebarsHooks();

  /**
   * 模板文件后缀
   *
   * @author chitanda
   * @date 2021-12-29 16:12:28
   */
  extension = 'hbs';

  /**
   * 模板宏文件夹名称，在项目根目录
   *
   * @author chitanda
   * @date 2021-12-29 16:12:54
   */
  macroFolderName = '@macro';

  /**
   * Creates an instance of HandlebarsEngine.
   *
   * @author chitanda
   * @date 2021-12-29 16:12:40
   */
  constructor() {
    this.init();
  }

  /**
   * 初始化
   *
   * @author chitanda
   * @date 2021-12-29 16:12:35
   * @protected
   */
  protected init(): void {
    installHelpers();
    this.hooks.init.call(this);
  }

  /**
   * 注册额外助手
   *
   * @author chitanda
   * @date 2022-12-19 15:12:50
   * @param {string} tag
   * @param {((...args: unknown[]) => string | boolean)} call
   */
  registerHelper(tag: string, call: (...args: unknown[]) => string | boolean) {
    Handlebars.registerHelper(tag, call);
  }

  /**
   * 注册宏
   *
   * @author chitanda
   * @date 2021-12-29 16:12:16
   * @param {string} key 宏标识
   * @param {string} template 宏模板内容
   */
  registerMacro(key: string, template: string): void {
    key = winToUnixPath(key);
    Handlebars.registerPartial(key, template);
    if (key.endsWith(this.extension)) {
      key = key.replace(`.${this.extension}`, '');
      Handlebars.registerPartial(key, template);
    }
  }

  /**
   * 预编译模板
   *
   * @author chitanda
   * @date 2021-12-29 16:12:16
   * @param {string} template
   * @return {*}  {HandlebarsTemplateDelegate}
   */
  preparse(template: string): HandlebarsTemplateDelegate {
    return Handlebars.compile(template, this.compileOpt);
  }

  /**
   * 根据内容编译模板
   *
   * @author chitanda
   * @date 2021-12-29 16:12:25
   * @param {(HandlebarsTemplateDelegate | string)} template
   * @param {ModelData} data
   * @return {*}  {string}
   */
  parse(
    template: HandlebarsTemplateDelegate | string,
    data: ModelData,
  ): string {
    if (typeof template === 'string') {
      return Handlebars.compile(template, this.compileOpt)(
        data,
        this.runtimeOpt,
      );
    }
    return template(data, this.runtimeOpt);
  }

  /**
   * 编译模板获取所有使用到的上下文级参数标识
   *
   * @author chitanda
   * @date 2021-12-29 16:12:37
   * @param {string} template
   * @return {*}  {string[]}
   */
  parseContextTags(template: string): string[] {
    const program = Handlebars.parse(template);
    const set: Set<string> = new Set();
    this.calcTemplateTags(set, program.body);
    return Array.from(set);
  }

  /**
   * 计算编译模板中的参数标签
   *
   * @author chitanda
   * @date 2021-12-30 10:12:46
   * @protected
   * @param {Set<string>} set
   * @param {Record<string, any>[]} tags
   */
  protected calcTemplateTags(
    set: Set<string>,
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    tags: Record<string, any>[],
  ): void {
    tags.forEach(item => {
      switch (item.type) {
        case 'SubExpression':
          if (item.params?.length > 0) {
            this.calcTemplateTags(set, item.params);
          }
          return;
        case 'MustacheStatement':
        case 'PathExpression': {
          const tag = item.path ? item.path.original : item.original;
          if (HelperUtil.isHelperName(tag)) {
            if (item.params?.length > 0) {
              this.calcTemplateTags(set, item.params);
            }
            return;
          }
          if (this.tagReg.test(tag)) {
            set.add(tag);
          }
          break;
        }
        default:
      }
    });
  }

  /**
   * 直接工具模板和数据编译
   *
   * @author chitanda
   * @date 2021-12-29 16:12:01
   * @param {string} template
   * @param {ModelData} data
   * @return {*}  {string}
   */
  render(template: string, data: ModelData): string {
    return Handlebars.compile(template, this.compileOpt)(data, this.runtimeOpt);
  }
}
