import { HelperAnd } from './and/and';
import { HelperCamelCase } from './camel-case/camel-case';
import { HelperConcat } from './concat/concat';
import { HelperEq } from './eq/eq';
import { HelperGt } from './gt/gt';
import { HelperGte } from './gte/gte';
import { HelperEqPropertyValue } from './eq-property-value/eq-property-value';
import { HelperJson } from './json/json';
import { HelperLowerCase } from './lower-case/lower-case';
import { HelperLt } from './lt/lt';
import { HelperLte } from './lte/lte';
import { HelperNeq } from './neq/neq';
import { HelperNot } from './not/not';
import { HelperOr } from './or/or';
import { HelperPascalCase } from './pascal-case/pascal-case';
import { HelperPluralize } from './pluralize/pluralize';
import { HelperSnakeCase } from './snake-case/snake-case';
import { HelperSpinalCase } from './spinal-case/spinal-case';
import { HelperUpperCase } from './upper-case/upper-case';

/**
 * 安装自定义助手
 *
 * @author chitanda
 * @date 2021-12-24 14:12:05
 * @export
 */
export function installHelpers(): void {
  new HelperAnd();
  new HelperCamelCase();
  new HelperConcat();
  new HelperEq();
  new HelperGt();
  new HelperGte();
  new HelperJson();
  new HelperLowerCase();
  new HelperLt();
  new HelperLte();
  new HelperNeq();
  new HelperNot();
  new HelperOr();
  new HelperPascalCase();
  new HelperPluralize();
  new HelperSnakeCase();
  new HelperSpinalCase();
  new HelperUpperCase();
  new HelperEqPropertyValue();
}
