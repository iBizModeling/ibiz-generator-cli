import * as pluralize from 'pluralize';
import { HelperBase } from '../helper-base';

/**
 * 单词转换为复数
 *
 * @description 用法 {{pluralize word}}, 效果: myName => myNames
 * @author chitanda
 * @date 2021-12-24 15:12:15
 * @export
 * @class HelperPluralize
 * @extends {HelperBase}
 */
export class HelperPluralize extends HelperBase {
  constructor() {
    super('pluralize');
  }

  onExecute(param: string): string {
    if (!param) {
      return '';
    }
    return pluralize(param).toLowerCase();
  }
}
