import { HandlebarsEngine } from '../../src';

describe('handlebars 引擎测试', () => {
  // 模板引擎初始化
  const engine = new HandlebarsEngine();

  it('模板上下文标识编译测试', () => {
    {
      const template = `{{projectName}}-core/{{modules}}/{{entities}}/{{spinalCase entity.codeName}}/{{spinalCase (or module.codeName entity.codeName)}}.ts.hbs`;
      const tags = engine.parseContextTags(template);
      expect(tags).toEqual(['projectName', 'modules', 'entities']);
    }
    {
      const template = `app_{{apps}}/src/page/{{appModules}}/{{spinalCase pages@DEEDITVIEW}}/index.ts.hbs`.replace(/@/g, ':');
      const tags = engine.parseContextTags(template);
      expect(tags).toEqual(['apps', 'appModules', 'pages:DEEDITVIEW']);
    }
    {
      const template = `app_{{apps}}/src/widgets/{{appEntities}}/{{ctrls@GRID}}-grid/index.ts.hbs`.replace(/@/g, ':');
      const tags = engine.parseContextTags(template);
      expect(tags).toEqual(['apps', 'appEntities', 'ctrls:GRID']);
    }
  });
});
