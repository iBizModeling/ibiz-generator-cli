import { TemplateOutFile } from '.';
import { TemplateFile } from './file/template-file/template-file';
import { CompilerHooks } from './hooks';

/**
 * 编译器
 *
 * @author chitanda
 * @date 2021-12-21 16:12:28
 * @export
 * @class Compiler
 */
export class Compiler {
  readonly hooks: CompilerHooks = new CompilerHooks();

  /**
   * 执行编译
   *
   * @author chitanda
   * @date 2021-12-22 19:12:15
   * @param {TemplateFile[]} temps
   * @return {*}  {Promise<void>}
   */
  async run(temps: TemplateFile[]): Promise<void> {
    for (let i = 0; i < temps.length; i++) {
      const temp = temps[i];
      this.hooks.compile.call(temp);
    }
  }

  /**
   * 发布模板生成文件
   *
   * @author chitanda
   * @date 2021-12-23 18:12:41
   * @param {TemplateOutFile} out
   * @return {*}  {Promise<void>}
   */
  async pub(out: TemplateOutFile): Promise<void> {
    this.hooks.publish.call(out);
  }
}
/**
 * 模板编译器
 */
export const compiler = new Compiler();
