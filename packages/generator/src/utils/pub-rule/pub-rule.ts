import { logger, readFirstLine } from '@ibizlab/template-core';
import { existsSync } from 'fs-extra';

const skipRule = '/* pub-rule @skip-overwrite  */';

/**
 * 发布规则
 *
 * @author chitanda
 * @date 2023-02-08 10:02:50
 * @export
 * @class PubRule
 */
export class PubRule {
  /**
   * 判断是否跳过覆盖写入
   *
   * @author chitanda
   * @date 2023-02-08 10:02:55
   * @static
   * @param {string} filePath
   * @return {*}  {Promise<boolean>} true: 跳过文件写入, false: 覆盖写入
   */
  static async skipOverWrite(filePath: string): Promise<boolean> {
    if (existsSync(filePath)) {
      const fistLine = await readFirstLine(filePath);
      if (fistLine === skipRule) {
        logger.info('跳过文件', `${filePath}`);
        return true;
      }
    }
    return false;
  }
}
