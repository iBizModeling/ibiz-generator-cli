import { existsSync, readFileSync } from 'fs-extra';

/**
 * 忽略文件帮助类
 *
 * @author chitanda
 * @date 2021-12-21 10:12:14
 * @export
 * @class IgnoreHelp
 */
export class IgnoreHelp {
  /**
   * 配置文件内容
   *
   * @author chitanda
   * @date 2021-12-21 10:12:21
   * @protected
   */
  protected fileContent = '';

  /**
   * 忽略文件 or 文件夹规则
   *
   * @author chitanda
   * @date 2021-12-21 10:12:07
   * @protected
   * @type {string[]}
   */
  protected rules: string[] = [];

  /**
   * 忽略规则
   *
   * @author chitanda
   * @date 2021-12-21 12:12:59
   * @protected
   * @type {RegExp}
   */
  protected reg?: RegExp;

  /**
   * Creates an instance of IgnoreHelp.
   *
   * @author chitanda
   * @date 2021-12-21 10:12:25
   * @param {string} ignoreFilePath 配置文件名称
   */
  constructor(protected readonly ignoreFilePath: string) {
    this.readIgnoreFile();
  }

  /**
   * 读取配置文件
   *
   * @author chitanda
   * @date 2021-12-21 15:12:45
   * @protected
   */
  protected readIgnoreFile(): void {
    if (existsSync(this.ignoreFilePath)) {
      const content = readFileSync(this.ignoreFilePath, 'utf-8');
      if (content) {
        this.fileContent = content;
        const rules = content.split('\n').filter(item => item.trim() !== '');
        rules.forEach(rule => this.addRule(rule));
      }
    }
  }

  /**
   * 新增忽略规则
   *
   * @author chitanda
   * @date 2021-12-21 16:12:08
   * @param {string} rule
   */
  addRule(rule: string): void {
    this.rules.push(rule);
  }

  /**
   * 需要排除的目录条件
   *
   * @author chitanda
   * @date 2021-12-21 12:12:07
   * @return {*}  {string[]}
   */
  getRules(): string[] {
    return this.rules;
  }
}
