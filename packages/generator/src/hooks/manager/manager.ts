import { SyncWaterfallHook } from 'tapable';
import { TemplateContextItem, TemplateFile } from '../../file';

/**
 * 管理器钩子
 *
 * @author chitanda
 * @date 2021-12-22 17:12:52
 * @export
 * @class ManagerHooks
 */
export class ManagerHooks {
  /**
   * 读取完成模板文件
   *
   * @author chitanda
   * @date 2021-12-23 18:12:52
   */
  templateRead = new SyncWaterfallHook<[TemplateFile]>(['temp']);

  /**
   * 计算 temp 文件需要的上下文
   *
   * @author chitanda
   * @date 2021-12-24 13:12:39
   */
  calcTempContext = new SyncWaterfallHook<
    [TemplateContextItem[], TemplateFile]
  >(['contexts', 'temp']);
}
