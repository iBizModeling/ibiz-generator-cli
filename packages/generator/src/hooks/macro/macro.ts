import { SyncHook } from 'tapable';
import { TemplateFile } from '../../file';

/**
 * 宏钩子
 *
 * @author chitanda
 * @date 2021-12-22 19:12:49
 * @export
 * @class MacroHooks
 */
export class MacroHooks {
  /**
   * 宏加载完成
   *
   * @author chitanda
   * @date 2021-12-22 19:12:24
   */
  complete = new SyncHook<[TemplateFile[]]>(['macros']);
}
