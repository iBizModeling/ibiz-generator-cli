import { SyncHook } from 'tapable';
import { TemplateFile, TemplateOutFile } from '../../file';

/**
 * 编译器钩子
 *
 * @author chitanda
 * @date 2021-12-22 19:12:11
 * @export
 * @class CompilerHooks
 */
export class CompilerHooks {
  /**
   * 编译单个模板
   *
   * @author chitanda
   * @date 2021-12-22 19:12:13
   */
  compile = new SyncHook<[TemplateFile]>(['temp']);

  /**
   * 编译完成
   *
   * @author chitanda
   * @date 2021-12-22 19:12:03
   */
  publish = new SyncHook<[TemplateOutFile]>(['out']);
}
