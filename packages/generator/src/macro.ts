import { readFileSync } from 'fs-extra';
import { glob } from 'glob';
import { basename, dirname, join, normalize, sep } from 'path';
import { winToUnixPath } from '@ibizlab/template-core';
import { MacroHooks } from './hooks';
import { TemplateMacroFile } from './file';

/**
 * 宏管理器
 *
 * @author chitanda
 * @date 2021-12-21 13:12:50
 * @export
 * @class Macro
 */
export class Macro {
  readonly hooks: MacroHooks = new MacroHooks();

  /**
   * 模板文件
   *
   * @author chitanda
   * @date 2021-12-21 13:12:02
   * @type {TemplateMacroFile[]}
   */
  readonly files: TemplateMacroFile[] = [];

  /**
   * 分割后模板文件路径
   *
   * @author chitanda
   * @date 2021-12-20 18:12:51
   * @protected
   * @type {string[]}
   */
  protected folders: string[] = [];

  /**
   * 模板文件夹
   *
   * @author chitanda
   * @date 2021-12-23 10:12:59
   * @protected
   * @type {string}
   */
  protected tempFolder!: string;

  /**
   * 宏文件夹名称
   *
   * @author chitanda
   * @date 2021-12-23 10:12:05
   * @protected
   * @type {string}
   */
  protected macroFolderName!: string;

  /**
   * 模板扩展名
   *
   * @author chitanda
   * @date 2021-12-23 10:12:12
   * @protected
   * @type {string}
   */
  protected extension!: string;

  setOptions(opt: {
    tempFolder: string;
    macroFolderName: string;
    extension: string;
  }): void {
    if (opt.tempFolder) {
      this.folders = opt.tempFolder.split(sep);
    }
    this.tempFolder = opt.tempFolder;
    this.macroFolderName = opt.macroFolderName;
    this.extension = opt.extension;
  }

  /**
   * 扫描宏模板文件夹
   *
   * @author chitanda
   * @date 2021-12-21 14:12:35
   * @return {*}  {Promise<void>}
   */
  async scan(): Promise<void> {
    this.syncScan();
  }

  /**
   * 同步扫描宏模板文件夹
   *
   * @author chitanda
   * @date 2022-01-18 18:01:59
   */
  syncScan(): void {
    const globRule = winToUnixPath(
      normalize(
        join(
          this.tempFolder,
          this.macroFolderName,
          `**${sep}*.${this.extension}`,
        ),
      ),
    );
    const paths = glob.sync(globRule, {
      cwd: this.tempFolder,
      dot: true,
    });
    paths.forEach(fullPath => {
      fullPath = normalize(fullPath);
      const file = readFileSync(fullPath, 'utf-8');
      const folders = dirname(fullPath).split(sep);
      const path = folders.slice(this.folders.length).join(sep);
      const fileName = basename(fullPath);
      this.files.push(
        new TemplateMacroFile(
          this.extension,
          join(path, fileName),
          fullPath,
          fileName,
          file,
        ),
      );
    });
    this.hooks.complete.call(this.files);
  }
}
/**
 * 模板宏管理器
 */
export const macro = new Macro();
