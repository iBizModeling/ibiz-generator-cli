import { Options } from '@ibizlab/template-core';

/**
 * 生成器配置参数
 *
 * @author chitanda
 * @date 2021-12-22 18:12:53
 * @export
 * @class GeneratorOptions
 * @implements {Options}
 */
export class GeneratorOptions implements Options {
  modelFolder: string;

  tempFolder: string;

  outFolder: string;

  extension: string;

  macroFolderName: string;

  /**
   * Creates an instance of GeneratorOptions.
   *
   * @author chitanda
   * @date 2021-12-22 18:12:15
   * @param {Options} options
   */
  constructor(options: Options) {
    this.modelFolder = options.modelFolder;
    this.tempFolder = options.tempFolder;
    this.outFolder = options.outFolder;
    this.extension = options.extension || 'ftl';
    this.macroFolderName = options.macroFolderName || '@macro';
  }
}
