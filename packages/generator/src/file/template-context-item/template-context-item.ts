import * as pluralize from 'pluralize';
import { GeneratorConst } from '../../constants';

/**
 * 模板上下文项
 *
 * @author chitanda
 * @date 2021-12-24 13:12:03
 * @export
 * @class TemplateContextItem
 */
export class TemplateContextItem {
  /**
   * 标识
   *
   * @author chitanda
   * @date 2021-12-27 18:12:26
   * @type {string}
   */
  readonly tag: string;

  /**
   * 只有在名称为复数时有值
   *
   * @author chitanda
   * @date 2021-12-28 13:12:41
   * @type {string}
   */
  readonly singularTag?: string;

  /**
   * 参数名称
   *
   * @author chitanda
   * @date 2021-12-24 13:12:48
   * @type {string}
   */
  readonly name: string;

  /**
   * 非复数名称，当名称 name 为复数时，此参数才会有值
   *
   * @author chitanda
   * @date 2021-12-28 13:12:05
   * @type {string}
   */
  readonly singularName?: string;

  /**
   * 参数子类型
   *
   * @author chitanda
   * @date 2021-12-24 13:12:43
   * @type {string}
   */
  readonly subType?: string;

  /**
   * Creates an instance of TemplateContextItem.
   *
   * @author chitanda
   * @date 2021-12-24 13:12:18
   * @param {string} tag 参数标识
   */
  constructor(tag: string) {
    const arr = tag.split(GeneratorConst.pathReplaceSpecialTag);
    this.name = arr[0];
    this.tag = this.name;
    if (arr.length > 1) {
      this.subType = arr[1];
      this.tag += `${GeneratorConst.pathReplaceSpecialTag}${this.subType}`;
    }
    if (pluralize.isPlural(this.name)) {
      this.singularName = pluralize.singular(this.name);
      this.singularTag = this.singularName;
      if (this.subType) {
        this.singularTag += `${GeneratorConst.pathReplaceSpecialTag}${this.subType}`;
      }
    }
  }

  /**
   * 名称是否为复数
   *
   * @author chitanda
   * @date 2021-12-28 13:12:21
   * @return {*}  {boolean}
   */
  isPlural(): boolean {
    return pluralize.isPlural(this.name);
  }

  /**
   * 名称为单数形式
   *
   * @author chitanda
   * @date 2021-12-28 13:12:41
   * @return {*}  {boolean}
   */
  isSingular(): boolean {
    return pluralize.isSingular(this.name);
  }
}
