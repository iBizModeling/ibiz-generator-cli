/**
 * 输出文件
 *
 * @author chitanda
 * @date 2021-12-19 17:12:13
 * @export
 * @class TemplateOutFile
 */
export class TemplateOutFile {
  /**
   * Creates an instance of TemplateOutFile.
   *
   * @author chitanda
   * @date 2021-12-19 17:12:20
   * @param {string} folder 相对于模板目录的路径
   * @param {string} name 文件名称
   * @param {string} content 文件内容
   */
  constructor(
    public folder: string,
    public name: string,
    public content: string,
  ) {}
}
