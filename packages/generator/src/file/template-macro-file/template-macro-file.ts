import { TemplateFile } from '../template-file/template-file';

/**
 * 模板宏文件
 *
 * @author chitanda
 * @date 2021-12-23 14:12:06
 * @export
 * @class TemplateMacroFile
 * @extends {TemplateFile}
 */
export class TemplateMacroFile extends TemplateFile {}
