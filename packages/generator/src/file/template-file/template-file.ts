import { dirname } from 'path';
import { notNilEmpty } from 'qx-util';
import { TemplateContextItem } from '..';
import { GeneratorConst } from '../../constants';

/**
 * 模板文件
 *
 * @author chitanda
 * @date 2021-12-22 17:12:12
 * @export
 * @class TemplateFile
 */
export class TemplateFile {
  /**
   * 相对于模板目录的路径
   *
   * @author chitanda
   * @date 2021-12-19 16:12:35
   * @type {string}
   */
  folder: string;

  /**
   * 在操作系统中的路径
   *
   * @author chitanda
   * @date 2021-12-21 15:12:20
   * @type {string}
   */
  fullFolder: string;

  /**
   * 文件名称
   *
   * @author chitanda
   * @date 2021-12-21 14:12:39
   * @type {string}
   */
  name: string;

  /**
   * 模板域上下文
   *
   * @author chitanda
   * @date 2021-12-24 13:12:54
   * @type {TemplateContextItem[]}
   */
  contexts: TemplateContextItem[] = [];

  /**
   * 是否为模板文件
   *
   * @author chitanda
   * @date 2022-01-27 11:01:23
   * @type {boolean}
   */
  isTemplate: boolean = false;

  /**
   * Creates an instance of TemplateFile.
   * @author chitanda
   * @date 2021-12-19 16:12:50
   * @param {string} extension 文件扩展名称
   * @param {string} path 相对于模板目录的路径
   * @param {string} fullPath 文件的完整路径
   * @param {string} fileName 文件名称带扩展名
   * @param {string | unknown} content 文件内容
   */
  constructor(
    public extension: string,
    public path: string,
    public fullPath: string,
    public fileName: string,
    public content: string | unknown,
  ) {
    this.folder = dirname(path).replace(
      GeneratorConst.pathSpecialTag,
      GeneratorConst.pathReplaceSpecialTag,
    );
    this.fullFolder = dirname(fullPath);
    let name = fileName;
    if (notNilEmpty(extension) && fileName.endsWith(extension)) {
      name = name.replace(`.${extension}`, '');
    }
    this.name = name.replace(
      GeneratorConst.pathSpecialTag,
      GeneratorConst.pathReplaceSpecialTag,
    );
  }
}
