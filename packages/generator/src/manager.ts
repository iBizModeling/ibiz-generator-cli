import { pathExistsSync, readFileSync } from 'fs-extra';
import { GlobalConst, winToUnixPath } from '@ibizlab/template-core';
import { basename, dirname, extname, join, normalize, sep } from 'path';
import { glob } from 'glob';
import { TemplateContextItem, TemplateFile } from './file';
import { IgnoreHelp } from './ignore-help/ignore-help';
import { ManagerHooks } from './hooks';

/**
 * 模板管理器
 *
 * @author chitanda
 * @date 2021-12-20 17:12:12
 * @export
 * @class Manager
 */
export class Manager {
  readonly hooks: ManagerHooks = new ManagerHooks();

  /**
   * 所有模板文件
   *
   * @author chitanda
   * @date 2021-12-20 17:12:23
   * @type {TemplateFile[]}
   */
  readonly files: TemplateFile[] = [];

  /**
   * 分割后模板文件路径
   *
   * @author chitanda
   * @date 2021-12-20 18:12:51
   * @type {string[]}
   */
  readonly folders: string[] = [];

  /**
   * 忽略文件帮助类
   *
   * @author chitanda
   * @date 2021-12-21 10:12:17
   */
  ignore!: IgnoreHelp;

  /**
   * 模板文件夹
   *
   * @author chitanda
   * @date 2021-12-23 10:12:16
   * @protected
   * @type {string}
   */
  protected folder!: string;

  /**
   * 模板文件扩展名
   *
   * @author chitanda
   * @date 2021-12-23 10:12:44
   * @protected
   * @type {string}
   */
  protected extension!: string;

  setOptions(opt: { folder: string; extension: string }): void {
    if (opt.folder) {
      this.folders.splice(0, this.folders.length);
      this.folders.push(...opt.folder.split(sep));
    }
    this.folder = opt.folder;
    this.extension = opt.extension;
    this.ignore = new IgnoreHelp(join(this.folder, GlobalConst.IGNORE_FILES));
  }

  /**
   * 扫描模板目录，并且读取链接目录 or 文件
   *
   * @author chitanda
   * @date 2021-12-20 17:12:03
   * @return {*}
   */
  async scan(): Promise<TemplateFile[]> {
    if (pathExistsSync(this.folder)) {
      this.files.splice(0, this.files.length);
      this.readFiles();
      return this.files;
    }
    throw new Error(`模板目录不存在: ${this.folder}`);
  }

  /**
   * 扫描单个模板文件
   *
   * @author chitanda
   * @date 2022-08-08 15:08:33
   * @param {string} templateFilePath 文件在操作系统中的路径
   * @return {*}  {Promise<TemplateFile>}
   */
  async scanSingle(templateFilePath: string): Promise<TemplateFile> {
    if (pathExistsSync(templateFilePath)) {
      const tempFile = this.createTemplateFile(templateFilePath);
      const i = this.files.findIndex(
        item => item.fullPath === tempFile.fullPath,
      );
      if (i !== -1) {
        this.files.splice(i, 1, tempFile);
      } else {
        this.files.push(tempFile);
      }
      return tempFile;
    }
    throw new Error(`模板文件不存在: ${templateFilePath}`);
  }

  /**
   * 删除单一模板文件
   *
   * @author chitanda
   * @date 2022-08-08 17:08:50
   * @param {string} fullPath
   * @return {*}  {(TemplateFile | undefined)}
   */
  remove(fullPath: string): TemplateFile | undefined {
    const i = this.files.findIndex(item => item.fullPath === fullPath);
    if (i !== -1) {
      const tempFile = this.files[i];
      this.files.splice(i, 1);
      return tempFile;
    }
  }

  /**
   * 使用 glob 规则递归读取模板文件，并且读取链接目录 or 文件
   *
   * @author chitanda
   * @date 2021-12-19 23:12:22
   * @protected
   * @return {*}  {void}
   */
  protected readFiles(): void {
    const folder = winToUnixPath(this.folder);
    const globRule = `${folder}/**`;
    const rules = this.ignore.getRules();
    const ignorePaths = rules.map(rule =>
      winToUnixPath(normalize(join(folder, rule))),
    );
    const paths = glob.sync(globRule, {
      cwd: folder,
      nodir: true,
      dot: true,
      ignore: ignorePaths,
    });
    paths.forEach((fullPath: string) => {
      const tempFile = this.createTemplateFile(fullPath);
      this.files.push(tempFile);
    });
  }

  /**
   * 创建模板文件
   *
   * @author chitanda
   * @date 2021-12-22 10:12:28
   * @protected
   * @param {string} fullPath 文件在操作系统中的路径
   * @return {*}  {TemplateFile}
   */
  protected createTemplateFile(fullPath: string): TemplateFile {
    fullPath = normalize(fullPath);
    const folders = dirname(fullPath).split(sep);
    const path = folders.slice(this.folders.length).join(sep);
    const fileName = basename(fullPath);
    const fileContent = readFileSync(fullPath, 'utf-8');
    const fileExt = extname(fullPath);
    const tempFile = new TemplateFile(
      fileExt.replace('.', ''),
      join(path, fileName),
      fullPath,
      fileName,
      fileContent,
    );
    if (tempFile.extension === this.extension) {
      tempFile.isTemplate = true;
    }
    const ctxs: TemplateContextItem[] = [];
    this.hooks.calcTempContext.call(ctxs, tempFile);
    tempFile.contexts = ctxs;
    return this.hooks.templateRead.call(tempFile);
  }
}
/**
 * 模板管理器
 */
export const manager = new Manager();
