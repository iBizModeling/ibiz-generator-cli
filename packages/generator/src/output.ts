import { existsSync, mkdirSync, writeFileSync } from 'fs-extra';
import { join } from 'path';
import { logger } from '@ibizlab/template-core';
import { OperationHooks } from './hooks';
import { TemplateOutFile } from './file';
import { PubRule } from './utils';

/**
 * 输出操作
 *
 * @author chitanda
 * @date 2021-12-21 19:12:53
 * @export
 * @class Output
 */
export class Output {
  readonly hooks: OperationHooks = new OperationHooks();

  /**
   * 成果物输出目录
   *
   * @author chitanda
   * @date 2021-12-23 10:12:24
   * @protected
   * @type {string}
   */
  protected outFolder!: string;

  setOptions(opt: { outFolder: string }): void {
    this.outFolder = opt.outFolder;
  }

  /**
   * 写编译后文件
   *
   * @author chitanda
   * @date 2021-12-20 18:12:19
   * @param {TemplateOutFile} out
   */
  async write(out: TemplateOutFile): Promise<TemplateOutFile> {
    const folder = join(this.outFolder, out.folder.replace(/\./g, '/'));
    if (!existsSync(folder)) {
      mkdirSync(folder, { recursive: true });
    }
    const filePath = join(folder, out.name);
    try {
      if (await PubRule.skipOverWrite(filePath)) {
        return out;
      }
      writeFileSync(filePath, out.content, 'utf-8');
      logger.info('生成文件', `${filePath}`);
    } catch (err) {
      throw new Error(`写入文件异常: ${filePath},${err.message}`);
    }
    return out;
  }
}
// 唯一实例
export const output = new Output();
