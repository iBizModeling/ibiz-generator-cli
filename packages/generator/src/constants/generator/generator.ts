/**
 * 编译器常量
 *
 * @author chitanda
 * @date 2021-12-24 12:12:29
 * @export
 * @enum {number}
 */
export class GeneratorConst {
  /**
   * 路径拼接特殊标签
   */
  static readonly pathSpecialTag = '@';

  /**
   * 路径特殊标签替换
   */
  static readonly pathReplaceSpecialTag = ':';
}
