import { Options } from '@ibizlab/template-core';
import { manager } from './manager';
import { macro } from './macro';
import { output } from './output';
import { GeneratorOptions } from './options/generator-options';
import { compiler } from './compiler';

/**
 * 生成器
 *
 * @author chitanda
 * @date 2021-12-21 14:12:29
 * @export
 * @class Generator
 */
export class Generator {
  /**
   * 配置参数
   *
   * @author chitanda
   * @date 2021-12-22 18:12:36
   * @protected
   * @type {GeneratorOptions}
   */
  protected opt!: GeneratorOptions;

  /**
   * 设置参数
   *
   * @author chitanda
   * @date 2021-12-22 18:12:28
   * @param {Options} opt
   */
  setOptions(opt: Options): void {
    this.opt = new GeneratorOptions(opt);
    macro.setOptions({
      tempFolder: this.opt.tempFolder,
      macroFolderName: this.opt.macroFolderName,
      extension: this.opt.extension,
    });

    manager.setOptions({
      folder: this.opt.tempFolder,
      extension: this.opt.extension,
    });
    manager.ignore.addRule(`**/${this.opt.macroFolderName}/**`);

    output.setOptions({
      outFolder: this.opt.outFolder,
    });
    this.taps();
  }

  /**
   * 订阅钩子
   *
   * @author chitanda
   * @date 2021-12-22 19:12:47
   * @protected
   */
  protected taps(): void {
    const tag = 'Generator';
    compiler.hooks.publish.tap(tag, output.write.bind(output));
  }

  /**
   * 执行
   *
   * @author chitanda
   * @date 2021-12-22 18:12:21
   * @param {Options} [opt]
   * @return {*}  {Promise<void>}
   */
  async execute(opt?: Options): Promise<void> {
    if (opt) {
      this.setOptions(opt);
    }
    if (!this.opt) {
      throw new Error('请先设置参数');
    }
    await macro.scan();
    await manager.scan();
    await compiler.run(manager.files);
  }
}
/**
 * 发布器
 */
export const generator = new Generator();
