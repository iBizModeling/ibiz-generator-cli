/* eslint-disable no-shadow */
/**
 * 全局静态变量
 *
 * @author chitanda
 * @date 2021-12-21 10:12:56
 * @export
 * @enum {string}
 */
export const enum GlobalConst {
  /**
   * 忽略文件 or 文件夹，配置文件
   */
  IGNORE_FILES = '.ibizlab-generator-ignore',
}
