/* eslint-disable @typescript-eslint/no-explicit-any */
import * as npmLog from 'npmlog';

/**
 * 全局日志输出
 *
 * @author chitanda
 * @date 2021-12-19 23:12:49
 * @export
 * @class GlobalLog
 */
export class GlobalLog {
  protected log = npmLog;

  constructor() {
    // 设置日志输出级别
    (npmLog as any).level = process.env.LOG_LEVEL || 'info';
    // 添加自定义命令
    // npmLog.addLevel('success', 2000, { fg: 'green', bold: true });
  }

  silly(prefix: string, message: string, ...args: any[]): void {
    this.log.silly(prefix, message, ...args);
  }

  verbose(prefix: string, message: string, ...args: any[]): void {
    this.log.verbose(prefix, message, ...args);
  }

  info(prefix: string, message: string, ...args: any[]): void {
    this.log.info(prefix, message, ...args);
  }

  timing(prefix: string, message: string, ...args: any[]): void {
    this.log.timing(prefix, message, ...args);
  }

  http(prefix: string, message: string, ...args: any[]): void {
    this.log.http(prefix, message, ...args);
  }

  notice(prefix: string, message: string, ...args: any[]): void {
    this.log.notice(prefix, message, ...args);
  }

  warn(prefix: string, message: string, ...args: any[]): void {
    this.log.warn(prefix, message, ...args);
  }

  error(prefix: string, message: string, ...args: any[]): void {
    this.log.error(prefix, message, ...args);
  }

  silent(prefix: string, message: string, ...args: any[]): void {
    this.log.silent(prefix, message, ...args);
  }

  debug(prefix: string, message: string, ...args: any[]): void {
    this.log.silly(prefix, message, ...args);
  }

  // success(prefix: string, message: string, ...args: any[]): void {
  //   this.log.success(prefix, message, ...args);
  // }
}
// 全局日志工具类
export const logger = new GlobalLog();
