/**
 * win 路径 \\ 转 linux 路径 /
 *
 * @author chitanda
 * @date 2022-01-19 09:01:23
 * @export
 * @param {string} path
 * @return {*}  {string}
 */
export function winToUnixPath(path: string): string {
  return path.replace(/\\/g, '/');
}

/**
 * linux 路径 / 转 win 路径 \\
 *
 * @author chitanda
 * @date 2022-01-19 10:01:34
 * @export
 * @param {string} path
 * @return {*}  {string}
 */
export function unixToWinPath(path: string): string {
  return path.replace(/\//g, '\\');
}
