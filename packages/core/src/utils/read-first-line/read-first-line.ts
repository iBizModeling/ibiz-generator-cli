import { createReadStream, statSync } from 'fs-extra';
import { createInterface } from 'readline';

/**
 * 读取指定文件第一行
 *
 * @author chitanda
 * @date 2023-02-08 10:02:28
 * @export
 * @param {string} filePath
 * @return {*}  {Promise<string>}
 */
export function readFirstLine(filePath: string): Promise<string> {
  return new Promise<string>(resolve => {
    const stat = statSync(filePath);
    if (stat.size > 0) {
      const rl = createInterface({
        input: createReadStream(filePath, 'utf-8'),
      });
      rl.once('line', line => {
        rl.close();
        resolve(line);
      });
    } else {
      resolve('');
    }
  });
}
