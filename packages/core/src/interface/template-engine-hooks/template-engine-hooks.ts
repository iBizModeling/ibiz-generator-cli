import { SyncHook } from 'tapable';
import { TemplateEngine } from '../template-engine/template-engine';

/**
 * 模板引擎需要实现的钩子
 *
 * @author chitanda
 * @date 2021-12-24 14:12:23
 * @export
 * @interface TemplateEngineHooks
 */
export interface TemplateEngineHooks {
  /**
   * 初始换
   *
   * @author chitanda
   * @date 2021-12-24 14:12:42
   * @type {SyncHook<TemplateEngine>}
   */
  init: SyncHook<TemplateEngine>;
}
