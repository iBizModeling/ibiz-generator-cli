import { ModelData } from '../model-data/model-data';
import { TemplateEngineHooks } from '../template-engine-hooks/template-engine-hooks';

/**
 * 模板引擎
 *
 * @description 主要负责对模板进行编译
 * @author chitanda
 * @date 2021-12-23 13:12:38
 * @export
 * @interface TemplateEngine
 */
export interface TemplateEngine {
  /**
   * 钩子
   *
   * @author chitanda
   * @date 2021-12-24 14:12:24
   * @type {TemplateEngineHooks}
   */
  hooks: TemplateEngineHooks;
  /**
   * 模板文件扩展名
   *
   * @author chitanda
   * @date 2021-12-23 14:12:18
   * @type {string}
   */
  extension: string;
  /**
   * 当前模板引擎宏文件夹名称
   *
   * @author chitanda
   * @date 2021-12-23 14:12:42
   * @type {string}
   */
  macroFolderName: string;
  /**
   * 注册模板宏
   *
   * @author chitanda
   * @date 2021-12-23 14:12:50
   * @param {string} key
   * @param {string | unknown} template
   */
  registerMacro(key: string, template: string | unknown): void;
  /**
   * 预编译
   *
   * @author chitanda
   * @date 2021-12-24 14:12:18
   * @param {string | unknown} template
   * @return {*}  {string | unknown}
   */
  preparse(template: string | unknown): string | unknown;
  /**
   * 编译模板文本
   *
   * @author chitanda
   * @date 2021-12-23 14:12:13
   * @param {string | unknown} template
   * @param {ModelData} data
   * @return {*}  {string}
   */
  parse(template: string | unknown, data: ModelData): string;
  /**
   * 获取所有上下文级参数标签
   *
   * @author chitanda
   * @date 2021-12-23 19:12:06
   * @param {string} template
   * @return {*}  {string}
   */
  parseContextTags(template: string): string[];
  /**
   * 根据字符串模板 和 数据模型 返回渲染后的字符串
   *
   * @author chitanda
   * @date 2021-12-28 17:12:18
   * @param {string} template
   * @param {ModelData} data
   * @return {*}  {string}
   */
  render(template: string, data: ModelData): string;
}
