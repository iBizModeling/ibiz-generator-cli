/**
 * 模板输出模型数据
 *
 * @author chitanda
 * @date 2021-12-23 13:12:49
 * @export
 * @interface ModelData
 */
export interface ModelData {
  [key: string]: unknown;
}
