/**
 * 核心配置参数
 *
 * @author chitanda
 * @date 2021-12-22 18:12:53
 * @export
 * @interface Options
 */
export interface Options {
  /**
   * 模型文件夹
   *
   * @author chitanda
   * @date 2021-12-22 18:12:00
   * @type {string}
   */
  modelFolder: string;
  /**
   * 模板文件
   *
   * @author chitanda
   * @date 2021-12-22 18:12:06
   * @type {string}
   */
  tempFolder: string;
  /**
   * 文件输出目录
   *
   * @author chitanda
   * @date 2021-12-22 18:12:13
   * @type {string}
   */
  outFolder: string;
  /**
   * 模板文件后缀
   *
   * @author chitanda
   * @date 2021-12-22 18:12:21
   * @type {string}
   */
  extension?: string;
  /**
   * 宏文件夹名称
   *
   * @author chitanda
   * @date 2021-12-22 18:12:27
   * @type {string}
   */
  macroFolderName?: string;
  /**
   * 发布模式，默认为系统
   *
   * @description 发布模式为应用时, modelFolder 为应用文件夹根目录
   * @default 'sys'
   * @author chitanda
   * @date 2022-08-05 15:08:16
   * @type {('sys' | 'app')}
   */
  mode?: 'sys' | 'app';
  /**
   * 应用标识，当模式为 app 时，此参数必填。为应用的 codeName
   *
   * @author chitanda
   * @date 2022-09-29 16:09:23
   * @type {string}
   */
  app?: string;
  /**
   * 是否为开发模式启动
   *
   * @author chitanda
   * @date 2022-08-08 19:08:10
   * @type {boolean}
   */
  dev?: boolean;
}
