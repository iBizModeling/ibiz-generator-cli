import * as chalk from 'chalk';
import { Command } from 'commander';

import { ERROR_PREFIX } from '../ui';
import { TemplatePreviewCommand } from './template/preview/preview';
import { TemplatePubCommand } from './template/pub/pub';

export class CommandLoader {
  public static load(program: Command): void {
    new TemplatePubCommand().load(program);
    new TemplatePreviewCommand().load(program);
    this.handleInvalidCommand(program);
  }

  private static handleInvalidCommand(program: Command) {
    program.on('command:*', () => {
      console.error(
        `\n${ERROR_PREFIX} Invalid command: ${chalk.red('%s')}`,
        program.args.join(' '),
      );
      console.log(
        `See ${chalk.red('--help')} for a list of available commands.\n`,
      );
      process.exit(1);
    });
  }
}
