import { TemplatePreview } from '@ibizlab/template';
import { Command } from 'commander';
import { pathExistsSync } from 'fs-extra';
import { normalize, resolve } from 'path';
import { ICommand } from '../../../interface';

/**
 * 命令输入参数
 *
 * @author chitanda
 * @date 2021-12-19 20:12:47
 * @export
 * @interface TemplatePreviewCommandOptions
 */
export interface TemplatePreviewCommandOptions {
  /**
   * 模型目录
   *
   * @author chitanda
   * @date 2021-12-19 20:12:24
   * @type {string}
   */
  model: string;
  /**
   * 模板目录
   *
   * @author chitanda
   * @date 2022-01-18 18:01:21
   * @type {string}
   */
  template: string;
  /**
   * 模板目录
   *
   * @author chitanda
   * @date 2021-12-19 20:12:32
   * @type {string}
   */
  file: string;
  /**
   * 需要预览的模型标识
   *
   * @author chitanda
   * @date 2022-01-17 17:01:36
   * @type {string}
   */
  modelId: string;
}

/**
 * 运行模板编译
 *
 * @author chitanda
 * @date 2021-12-18 15:12:19
 * @export
 * @class RunCommand
 * @implements {ICommand}
 */
export class TemplatePreviewCommand implements ICommand {
  load(program: Command): void {
    program
      .command('preview')
      .description('编译模板并运行')
      .option('-m, --model <model-path>', '模型目录')
      .option('-t, --template <template-path>', '模板目录')
      .option('-f, --file <template-file>', '需要预览的模板文件')
      .option('--model-id <model-id>', '指定需要预览的主模型标识')
      .action(this.action.bind(this));
  }

  async action(options: TemplatePreviewCommandOptions): Promise<void> {
    const { model, template, file, modelId } = options;
    const modelFolder = normalize(resolve(model));
    if (pathExistsSync(modelFolder) === false) {
      throw new Error(`模型目录不存在: ${modelFolder}`);
    }
    const templateFolder = normalize(resolve(template));
    if (pathExistsSync(templateFolder) === false) {
      throw new Error(`模板目录不存在: ${templateFolder}`);
    }
    const preview = new TemplatePreview(modelFolder, templateFolder);
    const result = preview.execute(file, modelId);
    if (process.send) {
      process.send({ type: 'preview', result });
    }
  }
}
