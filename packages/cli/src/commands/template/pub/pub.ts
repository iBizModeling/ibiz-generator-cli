import { Command } from 'commander';
import { template as t } from '@ibizlab/template';
import * as rm from 'rimraf';
import { normalize, resolve } from 'path';
import { ICommand } from '../../../interface';

/**
 * 命令输入参数
 *
 * @author chitanda
 * @date 2021-12-19 20:12:47
 * @export
 * @interface TemplatePubCommandOptions
 */
export interface TemplatePubCommandOptions {
  /**
   * 模型目录
   *
   * @author chitanda
   * @date 2021-12-19 20:12:24
   * @type {string}
   */
  model: string;
  /**
   * 模板目录
   *
   * @author chitanda
   * @date 2021-12-19 20:12:32
   * @type {string}
   */
  template: string;
  /**
   * 模板输出目录
   *
   * @author chitanda
   * @date 2021-12-19 20:12:37
   * @type {string}
   */
  output: string;
  /**
   * 指定发布的应用
   *
   * @author chitanda
   * @date 2022-08-05 15:08:01
   * @type {boolean}
   */
  app: string;
  /**
   * 是否开发模式启动
   *
   * @author chitanda
   * @date 2022-08-08 19:08:40
   * @type {boolean}
   */
  dev: boolean;
  /**
   * 清理输出目录
   *
   * @author chitanda
   * @date 2021-12-21 10:12:47
   * @type {boolean}
   */
  clean: boolean;
}

/**
 * 运行模板编译
 *
 * @author chitanda
 * @date 2021-12-18 15:12:19
 * @export
 * @class TemplatePubCommand
 * @implements {ICommand}
 */
export class TemplatePubCommand implements ICommand {
  load(program: Command): void {
    program
      .command('pub')
      .description('编译模板并运行')
      .option('-m, --model <model-path>', '模型目录')
      .option('-t, --template <template-path>', '模板目录')
      .option('-o, --output <output-path>', '输出目录, 默认输出: 模板目录/out')
      .option('--app [app-code-name]', '发布模式改为应用, 模型目录将识别为应用')
      .option('--dev', '开发模式启动')
      .option('--clean', '清理输出目录')
      .action(this.action.bind(this));
  }

  async action(options: TemplatePubCommandOptions): Promise<void> {
    const { model, template, output, app, dev, clean } = options;
    const modelFolder = normalize(resolve(model));
    const templateFolder = normalize(resolve(template));
    const outputFolder = normalize(resolve(output || `${templateFolder}/out`));
    if (clean === true) {
      rm.sync(`${outputFolder}/*`);
    }
    t.init({
      modelFolder,
      tempFolder: templateFolder,
      outFolder: outputFolder,
      mode: app ? 'app' : 'sys',
      app,
      dev,
    });
    await t.run();
  }
}
