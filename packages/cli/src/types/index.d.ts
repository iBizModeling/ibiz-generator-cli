declare module 'rimraf' {
  export function sync(p: string, options?: Record<string, unknown>): void;

  export function rimraf(
    p: string,
    options?: Record<string, undefined>,
    cb?: (...args) => void,
  ): void;

  export default rimraf;
}
