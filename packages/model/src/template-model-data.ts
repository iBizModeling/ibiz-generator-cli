import { kebabCase } from 'lodash';
import { DSLHelper } from '@ibiz/rt-model-api';
import {
  ApiEntityModel,
  ApiModel,
  AppEntityModel,
  AppModel,
  EntityModel,
  ModuleModel,
} from './model';
import { AppCodeList } from './model/app/app-code-list';
import { AppCounter } from './model/app/app-counter';
import { AppPlugin } from './model/app/app-plugin';
import { CtrlModel } from './model/app/ctrl-model';
import { EditorStyle } from './model/app/editor-style';
import { PageModel } from './model/app/page-model';
import { ViewStyle } from './model/app/view-style';
import { UILogic } from './model/app/ui-logic';
import { GlobalModel } from './model/global-model';
import { ApiDtoModel } from './model/system/api-dto-model';
import { ServicePathUtil } from './utils';

/**
 * 模板单个渲染对象
 *
 * @author chitanda
 * @date 2022-01-05 10:01:57
 * @export
 * @class TemplateModelData
 */
export class TemplateModelData {
  /**
   * 模型数据
   *
   * @author chitanda
   * @date 2022-01-05 10:01:11
   * @protected
   * @type {Record<string, unknown>}
   */
  protected readonly data: Record<string, unknown> = {};

  /**
   * dsl 转换帮助类
   *
   * @author chitanda
   * @date 2023-04-27 16:04:40
   * @protected
   */
  protected dslHelper = new DSLHelper();

  /**
   * 服务路径拼接工具
   *
   * @author zk
   * @date 2023-06-14 10:06:29
   * @protected
   * @type {ServicePathUtil}
   * @memberof TemplateModelData
   */
  protected servicePathUtil!: ServicePathUtil;

  /**
   * Creates an instance of TemplateModelData.
   *
   * @author chitanda
   * @date 2022-01-05 10:01:55
   * @param {GlobalModel} global
   */
  constructor(global: GlobalModel) {
    if (global) {
      Object.assign(this.data, global);
    }
    const appModel = this.data.app as AppModel;
    this.servicePathUtil = new ServicePathUtil(appModel);
  }

  /**
   * 新增模型项
   *
   * @author chitanda
   * @date 2022-01-05 10:01:14
   * @param {string} key
   * @param {unknown} val
   */
  set(key: string, val: unknown): void {
    this.data[key] = val;
  }

  /**
   * 合并模型
   *
   * @author chitanda
   * @date 2022-01-05 10:01:10
   * @param {unknown} obj
   */
  assign(obj: unknown): void {
    if (obj) {
      Object.assign(this.data, obj);
    }
  }

  /**
   * 判断属性是否存在
   *
   * @author chitanda
   * @date 2022-01-05 10:01:02
   * @param {string} key
   * @return {*}  {boolean}
   */
  exist(key: string): boolean {
    return !!this.data[key];
  }

  /**
   * 获取计算的模型
   *
   * @author chitanda
   * @date 2022-01-05 10:01:30
   * @return {*}  {Record<string, unknown>}
   */
  getModel(): Record<string, unknown> {
    return this.data;
  }

  /**
   * 设置当前发布类型
   *
   * @author chitanda
   * @date 2022-01-17 19:01:51
   * @param {string} type
   */
  setType(type: string): void {
    this.set('pubType', type);
  }

  /**
   * 设置模块数据
   *
   * @author chitanda
   * @date 2022-01-06 14:01:17
   * @param {ModuleModel} module
   */
  setModule(module: ModuleModel): void {
    this.set('module', module);
    this.set('modules', module.codeName.toLowerCase());
  }

  /**
   * 设置实体数据
   *
   * @author chitanda
   * @date 2022-01-06 14:01:33
   * @param {EntityModel} entity
   */
  setEntity(entity: EntityModel): void {
    this.set('entity', entity);
    const text = entity.codeName;
    this.set('entities', text);
    if (entity.type) {
      this.set(`entities:${entity.type}`, text);
    }
  }

  /**
   * 设置服务接口
   *
   * @author chitanda
   * @date 2022-01-06 14:01:45
   * @param {ApiModel} api
   */
  setApi(api: ApiModel): void {
    this.set('api', api);
    this.set('apis', api.codeName.toLowerCase());
  }

  /**
   * 设置接口实体
   *
   * @author chitanda
   * @date 2022-01-06 14:01:38
   * @param {ApiEntityModel} apiEntity
   */
  setApiEntity(apiEntity: ApiEntityModel): void {
    this.set('apiEntity', apiEntity);
    const text = apiEntity.codeName;
    this.set('apiEntities', text);
    if (apiEntity.type) {
      this.set(`apiEntities:${apiEntity.type}`, text);
    }
  }

  /**
   * 设置接口实体 DTO
   *
   * @author chitanda
   * @date 2022-01-07 16:01:15
   * @param {ApiDtoModel} apiDto
   */
  setApiDto(apiDto: ApiDtoModel): void {
    this.set('apiDto', apiDto);
    const text = apiDto.codeName;
    this.set('apiDtos', text);
    this.set(`apiDtos:${apiDto.type}`, text);
  }

  /**
   * 设置应用
   *
   * @author chitanda
   * @date 2022-01-06 14:01:42
   * @param {AppModel} app
   */
  setApp(app: AppModel): void {
    this.set('app', app);
    app.DSL.appId =`${app.sysCodeName.toLowerCase()}__${app.codeName.toLowerCase()}`;
    const text = app.codeName.toLowerCase();
    this.set('apps', text);
  }

  /**
   * 设置代码表
   *
   * @author chitanda
   * @date 2022-10-12 15:10:28
   * @param {AppCodeList} codeList
   */
  setCodeList(codeList: AppCodeList): void {
    this.set('appCodeList', codeList);
    const text = kebabCase(codeList.codeName).toLowerCase();
    this.set('appCodeLists', text);
  }

  /**
   * 设置计数器
   *
   * @author chitanda
   * @date 2022-10-12 15:10:51
   * @param {AppCounter} counter
   */
  setCounter(counter: AppCounter): void {
    this.set('appCounter', counter);
    const text = kebabCase(counter.codeName).toLowerCase();
    this.set('appCounters', text);
  }

  /**
   * 设置应用模块
   *
   * @author chitanda
   * @date 2022-01-06 14:01:57
   * @param {AppEntityModel} appEntity
   */
  setAppEntity(appEntity: AppEntityModel): void {
    this.set('appEntity', appEntity);
    if (appEntity.DSL && appEntity.DSL.requestPaths == null) {
      appEntity.DSL.requestPaths = this.servicePathUtil.calcRequestPaths(
        appEntity.DSL.id!,
      );
    }
    const text = kebabCase(appEntity.codeName).toLowerCase();
    this.set('appEntities', text);
  }

  /**
   * 设置界面
   *
   * @author chitanda
   * @date 2022-01-06 14:01:42
   * @param {PageModel} page
   */
  setPage(page: PageModel): void {
    this.set('page', page);
    const text = kebabCase(page.codeName).toLowerCase();
    this.set('pages', text);
    if (page.type) {
      this.set(`pages:${page.type}`, text);
    }
  }

  /**
   * 设置部件
   *
   * @author chitanda
   * @date 2022-01-06 15:01:41
   * @param {CtrlModel} ctrl
   */
  setCtrl(ctrl: CtrlModel): void {
    this.set('ctrl', ctrl);
    const text = kebabCase(ctrl.codeName).toLowerCase();
    this.set('ctrls', text);
    if (ctrl.type) {
      this.set(`ctrls:${ctrl.type}`, text);
    }
  }

  /**
   * 设置界面逻辑
   *
   * @param {UILogic} uiLogic
   * @memberof TemplateModelData
   */
  setUILogic(uiLogic: UILogic): void {
    this.set('uiLogic', uiLogic);
    const text = kebabCase(uiLogic.codeName).toLowerCase();
    this.set('uiLogics', text);
  }

  /**
   * 设置应用插件
   *
   * @param {AppPlugin} appPlugin
   * @memberof TemplateModelData
   */
  setAppPlugin(appPlugin: AppPlugin): void {
    this.set('appPlugin', appPlugin);
    const text = kebabCase(appPlugin.pluginCode).toLowerCase();
    this.set('appPlugins', text);
    if (appPlugin.type) {
      this.set(`appPlugins:${appPlugin.type}`, text);
    }
  }

  /**
   * 设置系统界面样式表
   *
   * @param {string} content
   * @memberof TemplateModelData
   */
  setSysCss(content: string): void {
    this.set('sysCss', content);
    this.set('sysCsses', 'sys-css');
  }

  /**
   * 设置编辑器样式
   *
   * @param {EditorStyle} editorStyle
   * @memberof TemplateModelData
   */
  setEditorStyle(editorStyle: EditorStyle) {
    this.set('editorStyle', editorStyle);
    const text = kebabCase(editorStyle.codeName).toLowerCase();
    this.set('editorStyles', text);
    if (editorStyle.type) {
      this.set(`editorStyles:${editorStyle.type}`, text);
    }
  }

  /**
   * 设置视图样式
   *
   * @param {ViewStyle} viewStyle
   * @memberof TemplateModelData
   */
  setViewStyle(viewStyle: ViewStyle) {
    this.set('viewStyle', viewStyle);
    const text = kebabCase(viewStyle.codeName).toLowerCase();
    this.set('viewStyles', text);
    if (viewStyle.type) {
      this.set(`viewStyles:${viewStyle.type}`, text);
    }
  }
}
