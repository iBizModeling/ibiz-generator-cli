import {
  IPSDEForm,
  IPSDEFormDetail,
  IPSDEFormGroupPanel,
  IPSDEFormMDCtrl,
  IPSDEWizardPanel,
  IPSModelService,
  PSModelServiceImpl,
} from '@ibizlab/model';
import { TemplateContextItem, TemplateFile } from '@ibizlab/template-generator';
import { readFileSync, pathExistsSync } from 'fs-extra';
import { join, resolve } from 'path';
import { notNilEmpty } from 'qx-util';
import { ContextModelTypeConst } from './constants';
import {
  AppEntityModel,
  AppGlobalModel,
  AppModel,
  CtrlModel,
  GlobalModel,
  PageModel,
  SysGlobalModel,
  SystemModel,
} from './model';
import { TemplateModelData } from './template-model-data';

/**
 * 模板模型
 *
 * @author chitanda
 * @date 2021-12-27 14:12:44
 * @export
 * @class TemplateModel
 */
export class TemplateModel {
  /**
   * 模型服务
   *
   * @author chitanda
   * @date 2021-12-27 14:12:53
   * @type {IPSModelService}
   */
  service: IPSModelService;

  /**
   * 当前发布系统
   *
   * @author chitanda
   * @date 2022-01-06 14:01:58
   * @readonly
   * @type {SystemModel}
   */
  system!: SystemModel;

  /**
   * 发布模式为应用时, 当前发布的应用
   *
   * @author chitanda
   * @date 2022-01-06 14:01:07
   * @readonly
   * @type {AppModel}
   */
  app!: AppModel;

  /**
   * 全局模型
   *
   * @author chitanda
   * @date 2021-12-28 14:12:22
   * @protected
   * @type {GlobalModel}
   */
  protected globalModel!: GlobalModel;

  /**
   * 当前系统所有部件实例
   *
   * @author chitanda
   * @date 2023-03-21 15:03:18
   * @protected
   * @type {[AppModel, PageModel, CtrlModel][]}
   */
  protected controls: [AppModel, PageModel, CtrlModel][] = [];

  /**
   * 已经实例化的部件对象组，key为[应用标识:视图标识:部件标识]
   *
   * @author chitanda
   * @date 2023-03-21 15:03:44
   * @protected
   * @type {Map<string, CtrlModel>}
   */
  protected controlMap: Map<string, CtrlModel> = new Map();

  /**
   * 系统部件是否已经初始化过
   *
   * @author chitanda
   * @date 2023-03-21 15:03:59
   * @protected
   * @type {boolean}
   */
  protected isSysControlInit: boolean = false;

  /**
   * Creates an instance of TemplateModel.
   *
   * @author chitanda
   * @date 2022-01-05 16:01:18
   * @param {string} modelFolder 模型文件夹
   * @param {('sys' | 'app')} [mode='sys'] 发布模式: sys 系统、app 应用
   * @param {string} [appCodeName]
   */
  constructor(
    protected modelFolder: string,
    protected mode: 'sys' | 'app' = 'sys',
    protected appCodeName?: string,
  ) {
    this.service = new PSModelServiceImpl(this.readModelJson.bind(this));
    this.onInit();
  }

  /**
   * 读取模型 json 文件
   *
   * @author chitanda
   * @date 2022-01-05 16:01:21
   * @protected
   * @param {string} modelPath
   * @return {*}  {IModel}
   */
  protected readModelJson(modelPath: string): IModel {
    const path = join(this.modelFolder, modelPath);
    const content = readFileSync(path, 'utf-8');
    return JSON.parse(content);
  }

  /**
   * 初始化
   *
   * @author chitanda
   * @date 2022-01-05 17:01:34
   * @protected
   */
  protected onInit(): void {
    this.initModel();
  }

  /**
   * 初始化模型对象
   *
   * @author chitanda
   * @date 2022-08-09 21:08:40
   * @protected
   */
  protected initModel(): void {
    this.system = new SystemModel();
    const model = this.service.getRootModel();
    this.system.init(this.service, model.M.dynaModelFilePath, model.M);

    // 初始化跟模型
    if (this.mode === 'app') {
      const app = this.system.apps?.find(
        item => item.codeName === this.appCodeName,
      );
      if (!app) {
        throw new Error(`未在系统中找到应用: ${this.appCodeName}`);
      }
      this.app = app;
      this.globalModel = new AppGlobalModel(this.system, this.app);
    } else {
      this.globalModel = new SysGlobalModel(this.system);
    }
  }

  /**
   * 重置模型缓存
   *
   * @author chitanda
   * @date 2022-08-09 21:08:00
   * @protected
   */
  protected resetModel(): void {
    this.initModel();
  }

  /**
   * 根据模型文件路径中的上下文计算，计算模型发布对象.
   * 当无上下文时，默认为主发布对象[系统 or 应用]
   *
   * @author chitanda
   * @date 2021-12-27 18:12:41
   * @param {TemplateFile} file
   * @return {*}  {TemplateModelData[]}
   */
  getModelData(file: TemplateFile): TemplateModelData[] {
    const { contexts } = file;
    let ctx!: TemplateContextItem;
    for (let i = contexts.length - 1; i >= 0; i--) {
      if (ContextModelTypeConst.test(contexts[i].name)) {
        ctx = contexts[i];
        break;
      }
    }
    if (ctx) {
      return this.getModelDataByType(ctx.name, ctx.subType);
    }
    return this.getModelDataByType(
      this.mode === 'app' ? ContextModelTypeConst.APPS : '',
    );
  }

  /**
   * 获取指定类型的上下文模型数据
   *
   * @description type 具体类型: {@link ContextModelTypeConst}
   * @author chitanda
   * @date 2022-01-18 10:01:26
   * @param {string} type
   * @param {(string | null)} [subType=null]
   * @return {*}  {TemplateModelData[]}
   */
  getModelDataByType(
    type: string,
    subType: string | null = null,
  ): TemplateModelData[] {
    const models: TemplateModelData[] = [];
    const apps = this.mode === 'app' ? [this.app] : this.system.apps;
    if (this.mode === 'sys') {
      switch (type) {
        case ContextModelTypeConst.MODULES:
          this.system.modules.forEach(module => {
            const model = new TemplateModelData(this.globalModel);
            model.setModule(module);
            model.setType('module');
            models.push(model);
          });
          break;
        case ContextModelTypeConst.ENTITIES:
          this.system.modules.forEach(module => {
            module.entities.forEach(entity => {
              if (subType && subType !== entity.type) {
                return;
              }
              const model = new TemplateModelData(this.globalModel);
              model.setModule(module);
              model.setEntity(entity);
              model.setType('entity');
              models.push(model);
            });
          });
          break;
        case ContextModelTypeConst.APIS:
          this.system.apis.forEach(api => {
            const model = new TemplateModelData(this.globalModel);
            model.setApi(api);
            model.setType('api');
            models.push(model);
          });
          break;
        case ContextModelTypeConst.API_ENTITIES:
          this.system.apis.forEach(api => {
            api.apiEntities.forEach(apiEntity => {
              const model = new TemplateModelData(this.globalModel);
              model.setApi(api);
              model.setApiEntity(apiEntity);
              model.setType('apiEntity');
              models.push(model);
            });
          });
          break;
        case ContextModelTypeConst.API_DTOS:
          this.system.apis.forEach(api => {
            api.apiEntities.forEach(apiEntity => {
              apiEntity.apiDtos.forEach(apiDto => {
                if (subType && subType !== apiDto.type) {
                  return;
                }
                const model = new TemplateModelData(this.globalModel);
                model.setApi(api);
                model.setApiEntity(apiEntity);
                model.setApiDto(apiDto);
                model.setType('apiDto');
                models.push(model);
              });
            });
          });
          break;
        default:
      }
    }
    switch (type) {
      case ContextModelTypeConst.APPS:
        apps.forEach(app => {
          const filePath = resolve(
            this.modelFolder,
            `${app.dynaModelFilePath}.css`,
          );
          let content = '';
          if (pathExistsSync(filePath)) {
            content = readFileSync(filePath, 'utf-8');
          }
          const model = new TemplateModelData(this.globalModel);
          model.setApp(app);
          model.setSysCss(content);
          model.setType('app');
          models.push(model);
        });
        break;
      case ContextModelTypeConst.APP_CODE_LIST:
        apps.forEach(app => {
          app.appCodeLists.forEach(codeList => {
            const model = new TemplateModelData(this.globalModel);
            model.setApp(app);
            model.setType('appCodeList');
            model.setCodeList(codeList);
            models.push(model);
          });
        });
        break;
      case ContextModelTypeConst.APP_COUNTER:
        apps.forEach(app => {
          app.appCounters.forEach(counter => {
            const model = new TemplateModelData(this.globalModel);
            model.setApp(app);
            model.setType('appCounter');
            model.setCounter(counter);
            models.push(model);
          });
        });
        break;
      case ContextModelTypeConst.APP_ENTITIES:
        apps.forEach(app => {
          app.appEntities.forEach(appEntity => {
            const model = new TemplateModelData(this.globalModel);
            model.setApp(app);
            model.setAppEntity(appEntity);
            model.setType('appEntity');
            models.push(model);
          });
        });
        break;
      case ContextModelTypeConst.PAGES:
        apps.forEach(app => {
          app.pages.forEach(page => {
            if (subType && subType !== page.type) {
              return;
            }
            const model = new TemplateModelData(this.globalModel);
            model.setApp(app);
            if (page.appEntity) {
              model.setAppEntity(page.appEntity);
            }
            model.setPage(page);
            model.setType('page');
            models.push(model);
          });
        });
        break;
      case ContextModelTypeConst.CTRLS: {
        if (this.isSysControlInit === false) {
          apps.forEach(app => {
            app.pages.forEach(page => {
              this.deepFillControls(app, page, page.layoutPanelCtrls);
              this.deepFillControls(app, page, page.ctrls);
            });
          });
          this.isSysControlInit = true;
        }
        const controls = this.controls.filter(
          ([_app, _page, control]) => control.controlType === subType,
        );
        controls.forEach(([app, page, control]) => {
          const model = new TemplateModelData(this.globalModel);
          model.setApp(app);
          const entity = control.getPSAppDataEntity() as AppEntityModel;
          if (entity) {
            model.setAppEntity(entity);
          } else {
            model.set('appEntities', 'app');
          }
          model.setPage(page);
          model.setCtrl(control);
          model.setType('ctrl');
          models.push(model);
        });
        break;
      }
      case ContextModelTypeConst.UI_LOGIC: {
        apps.forEach(app => {
          app.appEntities.forEach(appEntity => {
            appEntity.deUILogics.forEach(uiLogic => {
              const model = new TemplateModelData(this.globalModel);
              model.setApp(app);
              model.setAppEntity(appEntity);
              model.setUILogic(uiLogic);
              model.setType('uiLogic');
              models.push(model);
            });
          });
        });
        break;
      }
      case ContextModelTypeConst.APP_PLUGIN: {
        apps.forEach(app => {
          app.appPlugins.forEach(appPlugin => {
            if (subType && subType !== appPlugin.type) {
              return;
            }
            const model = new TemplateModelData(this.globalModel);
            model.setApp(app);
            model.setAppPlugin(appPlugin);
            model.setType('appPlugin');
            models.push(model);
          });
        });
        break;
      }
      case ContextModelTypeConst.SYS_CSS: {
        apps.forEach(app => {
          const filePath = resolve(
            this.modelFolder,
            `${app.dynaModelFilePath}.css`,
          );
          if (pathExistsSync(filePath)) {
            const content = readFileSync(filePath, 'utf-8');
            const model = new TemplateModelData(this.globalModel);
            model.setApp(app);
            model.setSysCss(content);
            model.setType('sysCss');
            models.push(model);
          }
        });
        break;
      }
      case ContextModelTypeConst.EDITOR_STYLE: {
        apps.forEach(app => {
          app.editorStyles.forEach(editorStyle => {
            if (subType && subType !== editorStyle.type) {
              return;
            }
            const model = new TemplateModelData(this.globalModel);
            model.setApp(app);
            const plugin = editorStyle.getPSSysPFPlugin();
            if (plugin) {
              const appPlugin = app.appPlugins.find(
                item => item.pluginCode === plugin.pluginCode,
              );
              if (appPlugin) {
                model.setAppPlugin(appPlugin);
              }
            }
            model.setEditorStyle(editorStyle);
            model.setType('editorStyle');
            models.push(model);
          });
        });
        break;
      }
      case ContextModelTypeConst.VIEW_STYLE: {
        apps.forEach(app => {
          app.viewStyles.forEach(viewStyle => {
            if (subType && subType !== viewStyle.type) {
              return;
            }
            const model = new TemplateModelData(this.globalModel);
            model.setApp(app);
            const plugin = viewStyle.getPSSysPFPlugin();
            if (plugin) {
              const appPlugin = app.appPlugins.find(
                item => item.pluginCode === plugin.pluginCode,
              );
              if (appPlugin) {
                model.setAppPlugin(appPlugin);
              }
            }
            model.setViewStyle(viewStyle);
            model.setType('viewStyle');
            models.push(model);
          });
        });
        break;
      }
      default:
        // eslint-disable-next-line no-case-declarations
        const model = new TemplateModelData(this.globalModel);
        models.push(model);
    }
    return models;
  }

  /**
   * 递归查找当前应用所有部件对象
   *
   * @author fangZhiHao
   * @date 2023-03-20 21:03:39
   * @protected
   * @param {CtrlModel[]} controls
   */
  protected deepFillControls(
    app: AppModel,
    page: PageModel,
    controls: CtrlModel[],
  ): void {
    if (controls.length > 0) {
      controls.forEach(control => {
        const tag = `${app.modelPath}:${page.modelPath}:${
          notNilEmpty(control.modelPath) ? control.modelPath : control.codeName
        }${notNilEmpty(control.modeltype) ? control.modeltype : ''}`;
        if (this.controlMap.has(tag)) {
          return;
        }
        this.controlMap.set(tag, control);
        this.controls.push([app, page, control]);
        if (control.controlType === 'WIZARDPANEL') {
          const forms = (
            control as unknown as IPSDEWizardPanel
          ).getPSDEEditForms();
          if (forms) {
            this.deepFillControls(
              app,
              page,
              forms.map(form => new CtrlModel(form)),
            );
          }
          return;
        }
        if (control.controlType === 'FORM') {
          this.deepFillFormMDEditorControls(
            app,
            page,
            (control as unknown as IPSDEForm).getPSDEFormPages(),
          );
          return;
        }
        if (control.ctrls) {
          this.deepFillControls(app, page, control.ctrls);
        }
      });
    }
  }

  /**
   * 递归填充表单项多数据部件引用发布实例对象
   *
   * @author fangZhiHao
   * @date 2023-03-20 21:03:13
   * @protected
   * @param {(IPSDEFormDetail[] | null)} details
   */
  protected deepFillFormMDEditorControls(
    app: AppModel,
    page: PageModel,
    details: IPSDEFormDetail[] | null,
  ): void {
    if (details) {
      // 继续递归子查找多数据部件
      details.forEach(model => {
        const items = (model as IPSDEFormGroupPanel).getPSDEFormDetails?.();
        if (items && items.length > 0) {
          return this.deepFillFormMDEditorControls(app, page, items);
        }
      });
      // 查找当前层级的 mdCtrl 部件
      const mdControls = details.filter(model => {
        if (
          model.detailType === 'MDCTRL' &&
          (model as IPSDEFormMDCtrl).getContentPSControl()
        ) {
          return true;
        }
        return null;
      });
      if (mdControls && mdControls.length > 0) {
        // 将多数据部件的部件继续递归注册
        const contentControls = mdControls.map(
          item => (item as IPSDEFormMDCtrl).getContentPSControl()!,
        );
        if (contentControls) {
          this.deepFillControls(
            app,
            page,
            contentControls.map(item => new CtrlModel(item)),
          );
        }
      }
    }
  }

  /**
   * 更新模型数据
   *
   * @description 根据更新模式，直接删除 modelService 对应模型的缓存。并重置实例化的发布对象，发布对象中的模型数据从 modelService 的缓存中取
   * @author chitanda
   * @date 2022-08-09 11:08:49
   * @param {string} fullPath
   */
  update(fullPath: string): void {
    let modelPath: string = '';
    if (this.mode === 'app') {
      modelPath = fullPath.substring(fullPath.indexOf('PSSYSAPPS'));
    } else {
      modelPath = fullPath.replace(this.modelFolder, '');
    }
    if (this.service.getStore().delete(modelPath)) {
      this.resetModel();
    }
    this.controls = [];
    this.controlMap.clear();
    this.isSysControlInit = false;
  }
}
