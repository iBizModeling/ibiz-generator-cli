import { IPSAppSubViewTypeRef, PSAppSubViewTypeRefImpl } from '@ibizlab/model';

/**
 * 视图样式
 *
 * @export
 * @class ViewStyle
 * @extends {PSAppSubViewTypeRefImpl}
 */
export class ViewStyle extends PSAppSubViewTypeRefImpl {
  /**
   * 视图类型
   *
   * @readonly
   * @type {string}
   * @memberof ViewStyle
   */
  get type(): string {
    return this.viewType;
  }

  constructor(protected model: IPSAppSubViewTypeRef) {
    super();
    Object.assign(this, model);
    const handle: ProxyHandler<ViewStyle> = {
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      get(target: any, prop: string) {
        if (target[prop]) {
          return target[prop];
        }
        return target.model[prop];
      },
    };
    // eslint-disable-next-line no-constructor-return
    return new Proxy(this, handle);
  }
}
