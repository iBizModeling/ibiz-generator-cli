/* eslint-disable no-constructor-return */
import {
  IPSControl,
  IPSControlContainer,
  IPSDEDataView,
  IPSDEList,
  IPSSysCalendar,
  PSControlContainerImpl,
} from '@ibizlab/model';
import { isNilOrEmpty } from 'qx-util';
import { IDSLOut, IDsl } from '../../interface';
import { dslHelper } from '../../utils';

/**
 * 部件发布对象
 *
 * @author chitanda
 * @date 2022-01-06 11:01:28
 * @export
 * @class CtrlModel
 */
export class CtrlModel extends PSControlContainerImpl implements IDSLOut {
  protected dsl?: IDsl;

  get DSL(): IDsl {
    if (!this.dsl) {
      this.dsl = dslHelper.control(this.M);
    }
    return this.dsl;
  }

  get cls(): string {
    return this.model.cls;
  }

  /**
   * 部件类型
   *
   * @author chitanda
   * @date 2022-01-06 15:01:05
   * @readonly
   * @type {string}
   */
  get type(): string {
    return this.controlType;
  }

  /**
   * 部件的引用部件
   *
   * @author chitanda
   * @date 2022-01-06 15:01:15
   * @protected
   * @type {CtrlModel[]}
   */
  protected _ctrls: CtrlModel[] = [];

  get ctrls(): CtrlModel[] {
    if (isNilOrEmpty(this._ctrls)) {
      const container = this as unknown as IPSControlContainer;
      if (container.getPSControls) {
        const ctrls = container.getPSControls();
        if (ctrls) {
          ctrls.forEach(ctrl => {
            this._ctrls.push(new CtrlModel(ctrl));
            if (
              ctrl.controlType === 'LIST' ||
              ctrl.controlType === 'DATAVIEW'
            ) {
              const layoutPanel = (
                ctrl as IPSDEDataView | IPSDEList
              ).getItemPSLayoutPanel();
              if (layoutPanel) {
                this._ctrls.push(new CtrlModel(layoutPanel));
              }
            } else if (ctrl.controlType === 'CALENDAR') {
              const items = (ctrl as IPSSysCalendar).getPSSysCalendarItems();
              if (items) {
                items.forEach(item => {
                  const layout = item.getPSLayoutPanel();
                  if (layout) {
                    this._ctrls.push(new CtrlModel(layout));
                  }
                });
              }
            }
          });
        }
      }
    }
    return this._ctrls;
  }

  /**
   * Creates an instance of CtrlModel.
   *
   * @author chitanda
   * @date 2022-01-06 11:01:05
   * @param {IPSControl} model 已经实例化的部件
   */
  constructor(protected model: IPSControl) {
    super();
    Object.assign(this, model);
    const handle: ProxyHandler<CtrlModel> = {
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      get(target: any, prop: string) {
        if (target[prop]) {
          return target[prop];
        }
        return target.model[prop];
      },
    };
    return new Proxy(this, handle);
  }
}
