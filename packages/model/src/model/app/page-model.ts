/* eslint-disable no-constructor-return */
import {
  IPSAppView,
  IPSDEDataView,
  IPSDEList,
  IPSSysCalendar,
  IPSViewLayoutPanel,
  PSAppViewImpl,
} from '@ibizlab/model';
import { isNilOrEmpty } from 'qx-util';
import { clone } from 'lodash';
import { AppEntityModel } from './app-entity-model';
import { CtrlModel } from './ctrl-model';
import { IDSLOut, IDsl } from '../../interface';
import { dslHelper } from '../../utils';

/**
 * 视图发布对象
 *
 * @author chitanda
 * @date 2022-01-06 11:01:21
 * @export
 * @class PageModel
 */
export class PageModel extends PSAppViewImpl implements IDSLOut {
  protected dsl?: IDsl;

  /**
   * 经过处理的 dsl，视图特殊处理，去掉了布局面板 和 部件
   *
   * @author chitanda
   * @date 2023-06-14 16:06:08
   * @protected
   * @type {IDsl}
   */
  protected dslOut?: IDsl;

  get DSL(): IDsl {
    if (!this.dsl) {
      this.dsl = dslHelper.appView(this.M);
    }
    return this.dsl;
  }

  get DSLOut(): IDsl {
    if (!this.dslOut) {
      const m = clone(this.M);
      delete m.getPSViewLayoutPanel;
      delete m.getPSControls;
      this.dslOut = dslHelper.appView(m);
    }
    return this.dslOut;
  }

  /**
   * 视图类型
   *
   * @author chitanda
   * @date 2022-01-06 14:01:44
   * @readonly
   * @type {string}
   */
  get type(): string {
    return this.viewType;
  }

  /**
   * 视图布局面板
   *
   * @author chitanda
   * @date 2022-11-15 16:11:40
   * @readonly
   * @type {(IPSViewLayoutPanel | null)}
   */
  get layoutPanel(): IPSViewLayoutPanel | null {
    return this.getPSViewLayoutPanel();
  }

  /**
   * 应用实体
   *
   * @author chitanda
   * @date 2022-02-10 14:02:32
   * @protected
   * @type {(AppEntityModel | null)}
   */
  protected _appEntity: AppEntityModel | null = null;

  get appEntity(): AppEntityModel | null {
    if (!this._appEntity) {
      const module = this.psAppDataEntity;
      if (module != null) {
        const app = this.getParentPSModelObject('app.IPSApplication');
        const appEntityImpl = new AppEntityModel();
        appEntityImpl.init(
          module.getPSModelService(),
          module.modelPath!,
          module.M,
          app,
        );
        this._appEntity = appEntityImpl;
      }
    }
    return this._appEntity;
  }

  /**
   * 视图部件
   *
   * @author chitanda
   * @date 2022-01-06 15:01:15
   * @protected
   * @type {CtrlModel[]}
   */
  protected _ctrls: CtrlModel[] = [];

  get ctrls(): CtrlModel[] {
    if (isNilOrEmpty(this._ctrls)) {
      const ctrls = this.getPSControls();
      if (ctrls) {
        ctrls.forEach(ctrl => {
          this._ctrls.push(new CtrlModel(ctrl));
          if (ctrl.controlType === 'LIST' || ctrl.controlType === 'DATAVIEW') {
            const layoutPanel = (
              ctrl as IPSDEDataView | IPSDEList
            ).getItemPSLayoutPanel();
            if (layoutPanel) {
              this._ctrls.push(new CtrlModel(layoutPanel));
            }
          } else if (ctrl.controlType === 'CALENDAR') {
            const items = (ctrl as IPSSysCalendar).getPSSysCalendarItems();
            if (items) {
              items.forEach(item => {
                const layout = item.getPSLayoutPanel();
                if (layout) {
                  this._ctrls.push(new CtrlModel(layout));
                }
              });
            }
          }
        });
      }
    }
    return this._ctrls;
  }

  protected _layoutPanelCtrls: CtrlModel[] = [];

  /**
   * 视图布局面板部件
   *
   * @author chitanda
   * @date 2022-11-15 16:11:48
   * @readonly
   * @type {CtrlModel[]}
   */
  get layoutPanelCtrls(): CtrlModel[] {
    if (isNilOrEmpty(this._ctrls)) {
      const ctrls = this.getPSViewLayoutPanel()?.getPSControls();
      if (ctrls) {
        ctrls.forEach(ctrl => {
          this._ctrls.push(new CtrlModel(ctrl));
        });
      }
    }
    return this._ctrls;
  }

  /**
   * Creates an instance of PageModel.
   *
   * @author chitanda
   * @date 2022-01-06 11:01:19
   * @param {IPSAppView} model 已经实例化的视图
   */
  constructor(protected model: IPSAppView) {
    super();
    Object.assign(this, model);
    const handle: ProxyHandler<PageModel> = {
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      get(target: any, prop: string) {
        if (target[prop]) {
          return target[prop];
        }
        return target.model[prop];
      },
    };
    return new Proxy(this, handle);
  }
}
