import { IPSAppDEUILogic, PSAppUILogicImpl } from '@ibizlab/model';
import { IDSLOut, IDsl } from '../../interface';
import { dslHelper } from '../../utils';

/**
 * 界面逻辑
 *
 * @export
 * @class UILogic
 * @extends {PSAppUILogicImpl}
 */
export class UILogic extends PSAppUILogicImpl implements IDSLOut {
  protected dsl?: IDsl;

  get DSL(): IDsl {
    if (!this.dsl) {
      this.dsl = dslHelper.appUiLogic(this.M);
    }
    return this.dsl;
  }

  constructor(protected model: IPSAppDEUILogic) {
    super();
    Object.assign(this, model);
    const handle: ProxyHandler<UILogic> = {
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      get(target: any, prop: string) {
        if (target[prop]) {
          return target[prop];
        }
        return target.model[prop];
      },
    };
    // eslint-disable-next-line no-constructor-return
    return new Proxy(this, handle);
  }
}
