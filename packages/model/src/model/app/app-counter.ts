import { PSAppCounterImpl } from '@ibizlab/model';
import { dslHelper } from '../../utils';
import { IDSLOut, IDsl } from '../../interface';

/**
 * 应用计数器
 *
 * @author chitanda
 * @date 2022-10-12 15:10:46
 * @export
 * @class AppCounter
 * @extends {PSAppCounterImpl}
 */
export class AppCounter extends PSAppCounterImpl implements IDSLOut {
  protected dsl?: IDsl;

  get DSL(): IDsl {
    if (!this.dsl) {
      this.dsl = dslHelper.appCounter(this.M);
    }
    return this.dsl;
  }
}
