import { PSAppCodeListImpl } from '@ibizlab/model';
import { dslHelper } from '../../utils';
import { IDSLOut, IDsl } from '../../interface';

/**
 * 代码表
 *
 * @author chitanda
 * @date 2022-10-12 15:10:38
 * @export
 * @class AppCodeList
 * @extends {PSAppCodeListImpl}
 */
export class AppCodeList extends PSAppCodeListImpl implements IDSLOut {
  protected dsl?: IDsl;

  get DSL(): IDsl {
    if (!this.dsl) {
      this.dsl = dslHelper.appCodeList(this.M);
    }
    return this.dsl;
  }
}
