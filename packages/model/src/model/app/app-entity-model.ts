import {
  IPSAppDERS,
  IPSApplication,
  IPSControl,
  PSAppDataEntityImpl,
} from '@ibizlab/model';
import { AppModel } from './app-model';
import { CtrlModel } from './ctrl-model';
import { PageModel } from './page-model';
import { UILogic } from './ui-logic';
import { dslHelper, plural } from '../../utils';
import { IDSLOut, IDsl } from '../../interface';

/**
 * 应用实体
 *
 * @author chitanda
 * @date 2022-01-03 10:01:45
 * @export
 * @class AppEntityModel
 * @extends {PSAppDataEntityImpl}
 */
export class AppEntityModel extends PSAppDataEntityImpl implements IDSLOut {
  protected dsl?: IDsl;

  get DSL(): IDsl {
    if (!this.dsl) {
      this.dsl = dslHelper.appDataEntity(this.M);
      this.getPSModelService().getRootModel();
      this.dsl.codeName2 = plural(this.dsl.codeName);
    }
    return this.dsl;
  }

  /**
   * 实体下所有视图
   *
   * @author chitanda
   * @date 2021-12-30 11:12:19
   * @protected
   * @type {PageModel[]}
   */
  protected _pages: PageModel[] = [];

  get pages(): PageModel[] {
    if (this._pages.length === 0) {
      const app = this.getParentPSModelObject(
        'app.IPSApplication',
      ) as IPSApplication;
      const views = app.getAllPSAppViews();
      if (views) {
        views
          .filter(view => {
            if (view.refFlag === true) {
              const entity = view.getPSAppDataEntity();
              return entity?.id === this.id;
            }
            return false;
          })
          .forEach(view => {
            this._pages.push(new PageModel(view));
          });
      }
    }
    return this._pages;
  }

  /**
   * 应用实体所有部件
   *
   * @description 应用实体所有部件走独立 json 加载，不使用视图中的部件模型
   * @author chitanda
   * @date 2021-12-28 19:12:09
   * @protected
   * @type {CtrlModel[]}
   */
  protected _ctrls: CtrlModel[] = [];

  get ctrls(): CtrlModel[] {
    if (this._ctrls.length === 0) {
      const map: Map<string, CtrlModel> = new Map();
      const s = this.getPSModelService();
      this.pages.forEach(page => {
        const { ctrls } = page;
        if (ctrls) {
          ctrls.forEach(ctrl => {
            const entity = ctrl.getPSAppDataEntityMust();
            const key = `${entity.id}_${ctrl.id}`;
            if (map.has(key)) {
              return;
            }
            if (ctrl.modelPath) {
              const model = s.getModel(this, ctrl.modelPath);
              const modelObj = s.getPSModel4(
                this,
                'control.IPSControl',
                model,
                '',
              );
              ctrl = new CtrlModel(modelObj as IPSControl);
            }
            map.set(key, ctrl);
          });
        }
      });
      this._ctrls = Array.from(map.values());
    }
    return this._ctrls;
  }

  /**
   * 当前应用实体路径Map
   *
   * @type {Map<number, IPSAppDERS[]>}
   * @memberof AppEntityModel
   */
  private psAppDERSPathMap: Map<number, IPSAppDERS[]> = new Map<
    number,
    IPSAppDERS[]
  >();

  /**
   * 当前应用实体所有关系路径
   *
   * @protected
   * @type {string[][]}
   * @memberof AppEntityModel
   */
  protected _appDERSPaths: string[][] = [];

  /**
   * 当前应用实体所有关系路径
   *
   * @readonly
   * @type {string[][]}
   * @memberof AppEntityModel
   */
  get appDERSPaths(): string[][] {
    if (this._appDERSPaths.length === 0) {
      const appDERSs = this.getPSAppDERSs();
      if (appDERSs.length === 0) {
        return [];
      }
      appDERSs.forEach((appDERS: IPSAppDERS) => {
        if (appDERS.minorDECodeName === appDERS.majorDECodeName) {
          return;
        }
        const list: IPSAppDERS[] = [];
        const nIndex = this.psAppDERSPathMap.size;
        this.psAppDERSPathMap.set(nIndex, list);
        this.fillPSAppDERSPath(appDERS, list);
      });
      this._appDERSPaths = this.handlePSAppDERSPaths();
    }
    return this._appDERSPaths;
  }

  /**
   * 当前应用实体所有界面逻辑
   *
   * @protected
   * @type {UILogic[]}
   * @memberof AppEntityModel
   */
  protected _deUILogics: UILogic[] = [];

  get deUILogics() {
    if (this._deUILogics.length === 0) {
      const uiLogics = this.getAllPSAppDEUILogics();
      if (uiLogics) {
        uiLogics.forEach(uiLogic =>
          this._deUILogics.push(new UILogic(uiLogic)),
        );
      }
    }
    return this._deUILogics;
  }

  /**
   * 处理应用实体路径
   *
   * @private
   * @return {*}
   * @memberof AppEntityModel
   */
  private handlePSAppDERSPaths() {
    const pathResult: Array<Array<string>> = [];
    if (this.psAppDERSPathMap.size > 0) {
      const iterator = this.psAppDERSPathMap.values();
      let i = 0;
      while (i < this.psAppDERSPathMap.size) {
        const list = iterator.next().value;
        if (list && list.length > 0) {
          const path: Array<string> = [];
          list.forEach((appDERS: IPSAppDERS) => {
            path.push(appDERS.majorDECodeName);
          });
          pathResult.push(path);
        }
        i++;
      }
    }
    if (pathResult.length > 0) {
      pathResult.sort((a: Array<string>, b: Array<string>) => {
        return b.length - a.length;
      });
    }
    return pathResult;
  }

  /**
   * 获取指定应用实体所有关系
   *
   * @private
   * @param {string} [entityCodeName] 应用实体标识
   * @return {*}  {IPSAppDERS[]}
   * @memberof AppEntityModel
   */
  private getPSAppDERSs(entityCodeName?: string): IPSAppDERS[] {
    const app = this.getParentPSModelObject('app.IPSApplication') as AppModel;
    const appDERSs: IPSAppDERS[] = [];
    if (app && app.allPSAppDERSs && app.allPSAppDERSs.length > 0) {
      app.allPSAppDERSs.forEach((appDERS: IPSAppDERS) => {
        if (entityCodeName) {
          if (appDERS.minorDECodeName === entityCodeName) {
            appDERSs.push(appDERS);
          }
        } else if (appDERS.minorDECodeName === this.codeName) {
          appDERSs.push(appDERS);
        }
      });
    }
    return appDERSs;
  }

  /**
   * 填充指定应用实体所有关联路径
   *
   * @private
   * @param {IPSAppDERS} appDERS 关系
   * @param {IPSAppDERS[]} list 关系集合
   * @return {*}
   * @memberof AppEntityModel
   */
  private fillPSAppDERSPath(appDERS: IPSAppDERS, list: IPSAppDERS[]) {
    if (list.length > 0) {
      list.forEach((tempPSAppDERS: IPSAppDERS) => {
        if (appDERS.id === tempPSAppDERS.id) {
          console.warn(
            `应用实体${this.codeName}存在递归引用关系${appDERS.name}`,
          );
        }
      });
    }
    list.unshift(appDERS);
    const majorList: IPSAppDERS[] = this.getPSAppDERSs(appDERS.majorDECodeName);
    if (majorList.length === 0) return;
    const srcList: IPSAppDERS[] = [];
    list.forEach((_appDERS: IPSAppDERS) => {
      srcList.push(_appDERS);
    });
    let nIndex: number = 0;
    majorList.forEach((tempPSAppDERS: IPSAppDERS) => {
      if (tempPSAppDERS.majorDECodeName === tempPSAppDERS.minorDECodeName) {
        return;
      }
      if (nIndex === 0) {
        if (appDERS.majorDEMajor !== false) {
          // 主实体，备份当前路径
          const list2: IPSAppDERS[] = [];
          srcList.forEach((_appDERS: IPSAppDERS) => {
            list2.push(_appDERS);
          });
          const nIndex2 = this.psAppDERSPathMap.size;
          this.psAppDERSPathMap.set(nIndex2, list2);
        }
        // 继续主路径
        this.fillPSAppDERSPath(tempPSAppDERS, list);
      } else {
        // 克隆新路径
        const list2: IPSAppDERS[] = [];
        srcList.forEach((_appDERS: IPSAppDERS) => {
          list2.push(_appDERS);
        });
        const nIndex2 = this.psAppDERSPathMap.size;
        this.psAppDERSPathMap.set(nIndex2, list2);
        this.fillPSAppDERSPath(tempPSAppDERS, list2);
      }
      nIndex++;
    });
  }
}
