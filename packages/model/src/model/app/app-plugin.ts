import { IPSAppPFPluginRef, PSAppPFPluginRefImpl } from '@ibizlab/model';

/**
 * 应用插件
 *
 * @export
 * @class AppPlugin
 * @extends {PSAppPFPluginRefImpl}
 */
export class AppPlugin extends PSAppPFPluginRefImpl {
  /**
   * 插件类型
   *
   * @readonly
   * @type {string}
   * @memberof AppPlugin
   */
  get type(): string {
    return this.M.pluginType;
  }

  /**
   * 代码模板
   *
   * @readonly
   * @type {(string | undefined)}
   * @memberof AppPlugin
   */
  get code(): string | undefined {
    return this.M.templCode;
  }

  /**
   * 代码模板2
   *
   * @readonly
   * @type {(string | undefined)}
   * @memberof AppPlugin
   */
  get code2(): string | undefined {
    return this.M.templCode2;
  }

  /**
   * 代码模板3
   *
   * @readonly
   * @type {(string | undefined)}
   * @memberof AppPlugin
   */
  get code3(): string | undefined {
    return this.M.templCode3;
  }

  /**
   * 代码模板4
   *
   * @readonly
   * @type {(string | undefined)}
   * @memberof AppPlugin
   */
  get code4(): string | undefined {
    return this.M.templCode4;
  }

  constructor(protected model: IPSAppPFPluginRef) {
    super();
    Object.assign(this, model);
    const handle: ProxyHandler<AppPlugin> = {
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      get(target: any, prop: string) {
        if (target[prop]) {
          return target[prop];
        }
        return target.model[prop];
      },
    };
    // eslint-disable-next-line no-constructor-return
    return new Proxy(this, handle);
  }
}
