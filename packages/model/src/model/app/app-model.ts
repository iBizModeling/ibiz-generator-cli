import { PSApplicationImpl } from '@ibizlab/model';
import { AppCodeList } from './app-code-list';
import { AppCounter } from './app-counter';
import { AppEntityModel } from './app-entity-model';
import { AppPlugin } from './app-plugin';
import { EditorStyle } from './editor-style';
import { PageModel } from './page-model';
import { ViewStyle } from './view-style';
import { IDSLOut, IDsl } from '../../interface';
import { dslHelper } from '../../utils';

/**
 * 应用模型
 *
 * @author chitanda
 * @date 2021-12-27 17:12:55
 * @export
 * @class AppModel
 * @extends {PSApplicationImpl}
 */
export class AppModel extends PSApplicationImpl implements IDSLOut {
  protected dsl?: IDsl;

  get DSL(): IDsl {
    if (!this.dsl) {
      this.dsl = dslHelper.application(this.M);
    }
    return this.dsl;
  }

  /**
   * 应用实体
   *
   * @author chitanda
   * @date 2021-12-27 17:12:52
   * @readonly
   * @type {AppEntityModel[]}
   */
  protected _appEntities: AppEntityModel[] = [];

  get appEntities(): AppEntityModel[] {
    if (this._appEntities.length === 0) {
      const entities = this.getAllPSAppDataEntities();
      if (entities) {
        entities.forEach(entity => {
          const entityImpl = new AppEntityModel();
          entityImpl.init(
            entity.getPSModelService(),
            entity.modelPath!,
            entity.M,
            entity.parent,
          );
          this._appEntities.push(entityImpl);
        });
      }
    }
    return this._appEntities;
  }

  /**
   * 应用代码表
   *
   * @author chitanda
   * @date 2022-10-12 15:10:57
   * @protected
   * @type {AppCodeList[]}
   */
  protected _appCodeLists: AppCodeList[] = [];

  get appCodeLists(): AppCodeList[] {
    if (this._appCodeLists.length === 0) {
      const codeLists = this.getAllPSAppCodeLists();
      if (codeLists) {
        codeLists.forEach(codeList => {
          const codeListImpl = new AppCodeList();
          codeListImpl.init(
            codeList.getPSModelService(),
            codeList.modelPath,
            codeList.M,
            codeList.parent,
          );
          this._appCodeLists.push(codeListImpl);
        });
      }
    }
    return this._appCodeLists;
  }

  /**
   * 应用计数器
   *
   * @author chitanda
   * @date 2022-10-12 15:10:48
   * @protected
   * @type {AppCounter[]}
   */
  protected _appCounters: AppCounter[] = [];

  get appCounters(): AppCounter[] {
    if (this._appCounters.length === 0) {
      const counters = this.getAllPSAppCounters();
      if (counters) {
        counters.forEach(counter => {
          const counterImpl = new AppCounter();
          counterImpl.init(
            counter.getPSModelService(),
            counter.modelPath,
            counter.M,
            counter.parent,
          );
          this._appCounters.push(counterImpl);
        });
      }
    }
    return this._appCounters;
  }

  /**
   * 应用所有视图
   *
   * @author chitanda
   * @date 2021-12-27 18:12:43
   * @readonly
   * @type {PageModel[]}
   */
  protected _pages: PageModel[] = [];

  get pages(): PageModel[] {
    if (this._pages.length === 0) {
      const views = this.getAllPSAppViews();
      if (views && views.length > 0) {
        views
          .filter(view => view.refFlag === true)
          .forEach(view => {
            this._pages.push(new PageModel(view));
          });
      }
    }
    return this._pages || [];
  }

  /**
   * 应用所有插件
   *
   * @protected
   * @type {AppPlugin[]}
   * @memberof AppModel
   */
  protected _appPlugins: AppPlugin[] = [];

  get appPlugins() {
    if (this._appPlugins.length === 0) {
      const plugins = this.getAllPSAppPFPluginRefs();
      if (plugins && plugins.length > 0) {
        plugins.forEach(plugin => {
          this._appPlugins.push(new AppPlugin(plugin));
        });
      }
    }
    return this._appPlugins;
  }

  /**
   * 应用所有编辑器样式
   *
   * @protected
   * @type {EditorStyle[]}
   * @memberof AppModel
   */
  protected _editorStyles: EditorStyle[] = [];

  get editorStyles() {
    if (this._editorStyles.length === 0) {
      const styles = this.getAllPSAppEditorStyleRefs();
      if (styles && styles.length > 0) {
        styles.forEach(style => {
          this._editorStyles.push(new EditorStyle(style));
        });
      }
    }
    return this._editorStyles;
  }

  /**
   * 应用所有视图样式
   *
   * @protected
   * @type {ViewStyle[]}
   * @memberof AppModel
   */
  protected _viewStyles: ViewStyle[] = [];

  get viewStyles() {
    if (this._viewStyles.length === 0) {
      const styles = this.getAllPSAppSubViewTypeRefs();
      if (styles && styles.length > 0) {
        styles.forEach(style => {
          this._viewStyles.push(new ViewStyle(style));
        });
      }
    }
    return this._viewStyles;
  }
}
