import { IPSAppEditorStyleRef, PSAppEditorStyleRefImpl } from '@ibizlab/model';

/**
 * 编辑器样式
 *
 * @export
 * @class EditorStyle
 * @extends {PSAppEditorStyleRefImpl}
 */
export class EditorStyle extends PSAppEditorStyleRefImpl {
  /**
   * 编辑器类型
   *
   * @readonly
   * @type {string}
   * @memberof EditorStyle
   */
  get type(): string {
    return this.containerType;
  }

  constructor(protected model: IPSAppEditorStyleRef) {
    super();
    Object.assign(this, model);
    const handle: ProxyHandler<EditorStyle> = {
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      get(target: any, prop: string) {
        if (target[prop]) {
          return target[prop];
        }
        return target.model[prop];
      },
    };
    // eslint-disable-next-line no-constructor-return
    return new Proxy(this, handle);
  }
}
