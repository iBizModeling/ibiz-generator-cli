import { PSDEMethodDTOFieldImpl } from '@ibizlab/model';
import { isNilOrEmpty, notNilEmpty } from 'qx-util';

/**
 * DTO 属性
 *
 * @author chitanda
 * @date 2022-01-09 10:01:45
 * @export
 * @class MethodDtoFieldModel
 * @extends {PSDEMethodDTOFieldImpl}
 */
export class MethodDtoFieldModel extends PSDEMethodDTOFieldImpl {
  get jsonName(): string {
    return this.name.toLowerCase();
  }

  get logicName(): string {
    if (isNilOrEmpty(this.M.logicName)) {
      return this.getRefPSDataEntityMust().logicName;
    }
    return this.M.logicName;
  }

  get javaType(): string {
    if (this.type === 'DTO') {
      // return this.getRefPSDEMethodDTOMust().name;
    } else if (this.type === 'DTOS') {
      // return `List<${this.getRefPSDEMethodDTOMust().name}>`;
    } else if (this.type === 'SIMPLE') {
      // return `simple 类型暂未实现`;
      // return ``;PropType.findType(getPSDEMethodDTOField().getStdDataType()).java;
    } else if (this.type === 'SIMPLES') {
      // return `simples 类型暂未实现`;
      // return `List<%s>`;String.format('List<%s>', PropType.findType(getPSDEMethodDTOField().getStdDataType()).java);
    }
    return 'javaType 暂未实现';
  }

  get predefinedType(): boolean {
    if (this.getPSDEField()) {
      return notNilEmpty(this.getPSDEFieldMust().predefinedType);
    }
    return false;
  }

  get keyDEField(): boolean {
    if (this.getPSDEField()) {
      return this.getPSDEFieldMust().keyDEField;
    }
    return false;
  }
}
