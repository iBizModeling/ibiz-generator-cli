import { PSDataEntityImpl } from '@ibizlab/model';
import { FieldModel } from './field-model';

/**
 * 实体模型
 *
 * @author chitanda
 * @date 2021-12-27 19:12:54
 * @export
 * @class EntityModel
 * @extends {PSDataEntityImpl}
 */
export class EntityModel extends PSDataEntityImpl {
  get entityName(): string {
    return this.name;
  }

  get module(): string {
    return this.getPSSystemModuleMust().codeName.toLowerCase();
  }

  protected _fields: FieldModel[] = [];

  get fields(): FieldModel[] {
    if (this._fields.length === 0) {
      const fields = this.getAllPSDEFields();
      if (fields) {
        fields.forEach(item => {
          const field = new FieldModel();
          field.init(
            item.getPSModelService(),
            item.modelPath!,
            item.M,
            item.parent,
          );
          this._fields.push(field);
        });
      }
    }
    return this._fields;
  }

  /**
   * 模型子类型
   *
   * @author chitanda
   * @date 2021-12-27 19:12:52
   * @readonly
   * @type {string}
   */
  get type(): string {
    switch (this.storageMode) {
      case 0:
        return 'NONE';
      case 1:
        return 'SQL';
      case 2:
        return 'NoSQL';
      case 4:
        return 'ServiceAPI';
      default:
        return 'SQL';
    }
  }
}
