import { IPSSysSFPub, PSSystemImpl } from '@ibizlab/model';
import { isNilOrEmpty } from 'qx-util';
import { AppModel } from '../app/app-model';
import { ApiModel } from './api-model';
import { ModuleModel } from './module-model';

/**
 * 系统模型
 *
 * @author chitanda
 * @date 2021-12-27 14:12:41
 * @export
 * @class SystemModel
 */
export class SystemModel extends PSSystemImpl {
  /**
   * 发布对象
   *
   * @author chitanda
   * @date 2021-12-27 17:12:43
   * @type {IPSSysSFPub}
   */
  pub?: IPSSysSFPub;

  get codeName(): string {
    if (this.pub?.codeName) {
      return this.pub.codeName;
    }
    return this.M.codeName;
  }

  get name(): string {
    if (this.pub?.name) {
      return this.pub.name;
    }
    return this.M.name;
  }

  enableDS: boolean = false;

  enableES: boolean = false;

  enableMongo: boolean = false;

  enableMQ: boolean = false;

  enableOAuth2: boolean = false;

  enableGlobalTransaction: boolean = false;

  enableMysql: boolean = false;

  enableOracle: boolean = false;

  enableDameng: boolean = false;

  enablePostgreSQL: boolean = false;

  enableDyna: boolean = false;

  enableWorkflow: boolean = false;

  mqSubscribes: string[] = [];

  /**
   * 系统模块
   *
   * @author chitanda
   * @date 2022-01-05 18:01:19
   * @type {ModuleModel[]}
   */
  protected _modules: ModuleModel[] = [];

  get modules(): ModuleModel[] {
    if (isNilOrEmpty(this._modules)) {
      const modules = this.getAllPSSystemModules() || [];
      modules.forEach(item => {
        const module = new ModuleModel();
        module.init(
          item.getPSModelService(),
          item.modelPath!,
          item.M,
          item.parent,
        );
        this._modules.push(module);
      });
    }
    return this._modules;
  }

  /**
   * 系统服务接口
   *
   * @author chitanda
   * @date 2022-01-05 18:01:05
   * @type {ServiceApiModel[]}
   */
  protected _apis: ApiModel[] = [];

  get apis(): ApiModel[] {
    if (isNilOrEmpty(this._apis)) {
      const apis = this.getAllPSSysServiceAPIs() || [];
      apis.forEach(item => {
        const api = new ApiModel();
        api.init(
          item.getPSModelService(),
          item.modelPath!,
          item.M,
          item.parent,
        );
        this._apis.push(api);
      });
    }
    return this._apis;
  }

  /**
   * 系统应用
   *
   * @author chitanda
   * @date 2022-01-05 17:01:36
   * @type {AppModel[]}
   */
  protected _apps: AppModel[] = [];

  get apps(): AppModel[] {
    if (isNilOrEmpty(this._apps)) {
      const apps = this.getAllPSApps() || [];
      apps.forEach(item => {
        const app = new AppModel();
        app.init(
          item.getPSModelService(),
          item.modelPath!,
          item.M,
          item.parent,
        );
        this._apps.push(app);
      });
    }
    return this._apps;
  }

  protected onInit(): void {
    // 计算发布对象
    const pubs = this.getAllPSSysSFPubs();
    if (pubs && pubs.length > 0) {
      const pub = pubs.find(item => item.mainPSSysSFPub === true);
      if (pub) {
        this.pub = pub;
      } else {
        this.pub = pubs[0];
      }
    }
    // 计算启用的数据源
    this.getAllPSDataEntities()?.forEach(entity => {
      const { storageMode, dSLink } = entity;
      if ((storageMode === 1 || storageMode === 2) && dSLink === 'DEFAULT') {
        this.enableDS = true;
      }
      if (storageMode === 2) {
        this.enableMongo = true;
      }
      const wfs = entity.getAllPSDEWFs();
      if (wfs && wfs.length > 0) {
        this.enableWorkflow = true;
      }
      if (entity.userTag === 'elasticsearch') {
        this.enableES = true;
      }
      // 实体数据同步集合
      const dataSyncs = entity.getAllPSDEDataSyncs();
      if (dataSyncs && dataSyncs.length > 0) {
        this.enableMQ = true;
        dataSyncs.forEach(dataSync => {
          if (dataSync.getInPSSysDataSyncAgent()) {
            this.mqSubscribes.push(dataSync.codeName);
          }
        });
      }
      // 实体数据库配置
      const dbConfigs = entity.getAllPSDEDBConfigs();
      if (dbConfigs && dbConfigs.length > 0) {
        dbConfigs.forEach(dbConfig => {
          const dbType = dbConfig.dBType
            .toLowerCase()
            .replace('mysql5', 'mysql');
          switch (dbType) {
            case 'mysql':
              this.enableMysql = true;
              break;
            case 'oracle':
              this.enableOracle = true;
              break;
            case 'postgresql':
              this.enablePostgreSQL = true;
              break;
            case 'dameng':
              this.enableDameng = true;
              break;
            default:
            // No Default
          }
        });
      }
    });
  }
}
