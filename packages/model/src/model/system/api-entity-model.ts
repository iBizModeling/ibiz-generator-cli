import { ApiDtoModel } from './api-dto-model';
import { EntityModel } from './entity-model';

/**
 * 接口实体
 *
 * @author chitanda
 * @date 2022-01-05 18:01:37
 * @export
 * @class ApiEntityModel
 * @extends {PSAppDataEntityImpl}
 */
export class ApiEntityModel extends EntityModel {
  get api() {
    return this.parent;
  }

  // get entity() {
  //   return this.getPSModelService().getPSDataEntity(this.entityName);
  // }

  protected _apiDtos: ApiDtoModel[] = [];

  get apiDtos() {
    if (this._apiDtos.length === 0) {
      const dtos = this.getAllPSDEMethodDTOs();
      if (dtos) {
        dtos.forEach(item => {
          const dto = new ApiDtoModel();
          dto.init(
            item.getPSModelService(),
            item.modelPath!,
            item.M,
            item.parent,
          );
          this._apiDtos.push(dto);
        });
      }
    }
    return this._apiDtos;
  }
}
