import { PSDEFieldImpl } from '@ibizlab/model';
import { camelCase, eq } from 'lodash';

/**
 * 属性模型
 *
 * @author chitanda
 * @date 2022-01-05 14:01:53
 * @export
 * @class FieldModel
 * @extends {PSDEFieldImpl}
 */
export class FieldModel extends PSDEFieldImpl {
  get fieldName(): string {
    return this.name;
  }

  get columnName(): string {
    return this.fieldName.toLowerCase();
  }

  get alias(): string | null {
    const alias: string = camelCase(this.codeName);
    if (eq(this.columnName, alias) && this.dataType === 'PICKUP') {
      return null;
    }
    return alias;
  }

  get jsonName(): string {
    return this.codeName.toLowerCase();
  }

  protected _timeType: string = 'timeType 暂未支持';

  get timeType(): string {
    return this._timeType;
  }
}
