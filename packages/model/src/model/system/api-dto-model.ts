import { PSDEMethodDTOImpl } from '@ibizlab/model';
import { MethodDtoFieldModel } from './method-dto-field-model';

/**
 * 服务接口 DTO
 *
 * @author chitanda
 * @date 2022-01-07 17:01:27
 * @export
 * @class ApiDtoModel
 * @extends {PSDEMethodDTOImpl}
 */
export class ApiDtoModel extends PSDEMethodDTOImpl {
  get codeName(): string {
    return this.name;
  }

  get apiEntity() {
    return this.parent;
  }

  protected _apiDtoFields: MethodDtoFieldModel[] = [];

  get apiDtoFields() {
    if (this._apiDtoFields.length === 0) {
      const fields = this.getPSDEMethodDTOFields();
      if (fields) {
        fields.forEach(item => {
          const field = new MethodDtoFieldModel();
          field.init(
            item.getPSModelService(),
            item.modelPath,
            item.M,
            item.parent,
          );
          this._apiDtoFields.push(field);
        });
      }
    }
    return this._apiDtoFields;
  }
}
