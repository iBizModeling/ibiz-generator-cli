import { PSSystemModuleImpl } from '@ibizlab/model';
import { EntityModel } from './entity-model';

/**
 * 系统模块
 *
 * @author chitanda
 * @date 2021-12-27 18:12:23
 * @export
 * @class ModuleModel
 * @extends {PSSystemModuleImpl}
 */
export class ModuleModel extends PSSystemModuleImpl {
  /**
   * 模块实体
   *
   * @author chitanda
   * @date 2022-01-06 14:01:47
   * @readonly
   * @type {EntityModel[]}
   */
  protected _entities: EntityModel[] = [];

  get entities(): EntityModel[] {
    if (this._entities.length === 0) {
      const entities = this.getAllPSDataEntities();
      if (entities) {
        entities.forEach(item => {
          const entity = new EntityModel();
          entity.init(
            item.getPSModelService(),
            item.modelPath!,
            item.M,
            item.parent,
          );
          this._entities.push(entity);
        });
      }
    }
    return this._entities;
  }
}
