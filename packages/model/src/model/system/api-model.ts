import { PSSysServiceAPIImpl } from '@ibizlab/model';
import { ApiEntityModel } from './api-entity-model';

/**
 * 服务接口
 *
 * @author chitanda
 * @date 2021-12-30 11:12:16
 * @export
 * @class ApiModel
 * @extends {PSSysServiceAPIImpl}
 */
export class ApiModel extends PSSysServiceAPIImpl {
  /**
   * 所有 api 实体
   *
   * @author chitanda
   * @date 2021-12-30 11:12:37
   * @protected
   * @type {ApiEntityModel[]}
   */
  protected _apiEntities: ApiEntityModel[] = [];

  get apiEntities(): ApiEntityModel[] {
    if (this._apiEntities.length === 0) {
      const apis = this.getPSDEServiceAPIs();
      if (apis) {
        apis.forEach(api => {
          const entity = api.getPSDataEntityMust();
          const apiEntity = new ApiEntityModel();
          apiEntity.init(
            entity.getPSModelService(),
            entity.modelPath!,
            entity.M,
            entity.parent,
          );
          this._apiEntities.push(apiEntity);
        });
      }
    }
    return this._apiEntities;
  }
}
