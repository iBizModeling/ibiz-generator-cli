import { AppModel } from './app/app-model';
import { SysGlobalModel } from './sys-global-model';
import { SystemModel } from './system/system-model';

/**
 * 应用全局模型
 *
 * @author chitanda
 * @date 2022-08-05 18:08:52
 * @export
 * @class AppGlobalModel
 */
export class AppGlobalModel extends SysGlobalModel {
  constructor(
    public sys: SystemModel,
    public app: AppModel,
  ) {
    super(sys);
  }
}
