import { IPSSysSFPub } from '@ibizlab/model';
import { GlobalModel } from './global-model';
import { SystemModel } from './system/system-model';

/**
 * 系统全局模型
 *
 * @author chitanda
 * @date 2022-08-05 18:08:23
 * @export
 * @class SysGlobalModel
 * @extends {GlobalModel}
 */
export class SysGlobalModel extends GlobalModel {
  /**
   * 发布对象
   *
   * @author chitanda
   * @date 2021-12-27 17:12:43
   * @type {IPSSysSFPub}
   */
  pub?: IPSSysSFPub;

  /**
   * 项目名称
   *
   * @author chitanda
   * @date 2022-01-03 10:01:37
   * @type {string}
   */
  projectName: string;

  /**
   * 项目描述
   *
   * @author chitanda
   * @date 2022-01-03 10:01:52
   * @type {string}
   */
  projectDesc: string;

  /**
   * 包名称
   *
   * @author chitanda
   * @date 2022-01-03 10:01:35
   * @type {string}
   */
  packageName: string;

  /**
   * 包路径
   *
   * @author chitanda
   * @date 2022-01-05 18:01:46
   * @type {string}
   */
  packagePath: string;

  /**
   * Creates an instance of GlobalModel.
   *
   * @author chitanda
   * @date 2022-01-03 10:01:06
   * @param {SystemModel} system 系统发布模型
   */
  constructor(public system: SystemModel) {
    super();
    this.projectName = system.codeName.toLowerCase();
    this.projectDesc = system.name;
    this.packageName = this.projectName;
    // 计算发布对象
    const pubs = system.getAllPSSysSFPubs();
    if (pubs && pubs.length > 0) {
      const pub = pubs.find(item => item.mainPSSysSFPub === true);
      if (pub) {
        this.pub = pub;
      } else {
        const [item] = pubs;
        this.pub = item;
      }
      this.projectName = this.pub.codeName.toLowerCase();
      this.projectDesc = this.pub.name;
      this.packageName = this.pub.pKGCodeName;
    }
    // 包路径
    this.packagePath = this.packageName.replace(/\./g, '/');
  }
}
