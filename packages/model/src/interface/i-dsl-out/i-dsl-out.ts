/**
 * 模型对象补充 DSL 输出
 *
 * @author chitanda
 * @date 2023-06-14 16:06:28
 * @export
 * @interface IDSLOut
 */
export interface IDSLOut {
  /**
   * 转换后的 DSL 模型
   *
   * @author chitanda
   * @date 2023-06-14 16:06:17
   * @type {Record<string, unknown>}
   */
  DSL: Record<string, unknown>;
}
