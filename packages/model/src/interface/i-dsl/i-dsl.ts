/**
 * Dsl 模型对象接口
 *
 * @author chitanda
 * @date 2023-06-14 16:06:33
 * @export
 * @interface IDsl
 */
export interface IDsl {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [key: string | number | symbol]: any;
}
