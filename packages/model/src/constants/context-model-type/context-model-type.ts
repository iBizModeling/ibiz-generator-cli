/**
 * 支持的发布对象类型对应在上下文中的名称
 *
 * @author chitanda
 * @date 2022-01-05 19:01:58
 * @export
 * @class ContextModelTypeConst
 */
export class ContextModelTypeConst {
  /**
   * 系统级
   *
   * @author chitanda
   * @date 2022-01-18 10:01:49
   * @static
   */
  static readonly SYSTEM = 'system';

  /**
   * 系统模块
   *
   * @author chitanda
   * @date 2022-01-05 19:01:19
   * @static
   */
  static readonly MODULES = 'modules';

  /**
   * 实体
   *
   * @author chitanda
   * @date 2022-01-05 19:01:25
   * @static
   */
  static readonly ENTITIES = 'entities';

  /**
   * 服务接口
   *
   * @author chitanda
   * @date 2022-01-05 19:01:30
   * @static
   */
  static readonly APIS = 'apis';

  /**
   * 接口实体
   *
   * @author chitanda
   * @date 2022-01-05 19:01:35
   * @static
   */
  static readonly API_ENTITIES = 'apiEntities';

  /**
   * 接口实体 dto
   *
   * @author chitanda
   * @date 2022-01-07 16:01:47
   * @static
   */
  static readonly API_DTOS = 'apiDtos';

  /**
   * 应用
   *
   * @author chitanda
   * @date 2022-01-05 19:01:40
   * @static
   */
  static readonly APPS = 'apps';

  /**
   * 应用实体
   *
   * @author chitanda
   * @date 2022-01-05 19:01:52
   * @static
   */
  static readonly APP_ENTITIES = 'appEntities';

  /**
   * 应用代码表
   *
   * @author chitanda
   * @date 2022-10-12 14:10:46
   * @static
   */
  static readonly APP_CODE_LIST = 'appCodeLists';

  /**
   * 应用计数器
   *
   * @author chitanda
   * @date 2022-10-12 14:10:59
   * @static
   */
  static readonly APP_COUNTER = 'appCounters';

  /**
   * 应用视图
   *
   * @author chitanda
   * @date 2022-01-05 19:01:57
   * @static
   */
  static readonly PAGES = 'pages';

  /**
   * 部件发布对象
   *
   * @author chitanda
   * @date 2022-01-05 19:01:05
   * @static
   */
  static readonly CTRLS = 'ctrls';

  /**
   * 界面逻辑
   *
   * @static
   * @memberof ContextModelTypeConst
   */
  static readonly UI_LOGIC = 'uiLogics';

  /**
   * 应用插件
   *
   * @static
   * @memberof ContextModelTypeConst
   */
  static readonly APP_PLUGIN = 'appPlugins';

  /**
   * 系统界面样式表
   *
   * @static
   * @memberof ContextModelTypeConst
   */
  static readonly SYS_CSS = 'sysCsses';

  /**
   * 编辑器样式
   *
   * @static
   * @memberof ContextModelTypeConst
   */
  static readonly EDITOR_STYLE = 'editorStyles';

  /**
   * 视图样式
   *
   * @static
   * @memberof ContextModelTypeConst
   */
  static readonly VIEW_STYLE = 'viewStyles';

  /**
   * 测试是否为上下文发布对象参数
   *
   * @author chitanda
   * @date 2022-01-05 19:01:13
   * @param {string} tag
   * @return {*}  {boolean}
   */
  static test(tag: string): boolean {
    switch (tag) {
      case ContextModelTypeConst.MODULES:
      case ContextModelTypeConst.ENTITIES:
      case ContextModelTypeConst.APIS:
      case ContextModelTypeConst.API_ENTITIES:
      case ContextModelTypeConst.API_DTOS:
      case ContextModelTypeConst.APPS:
      case ContextModelTypeConst.APP_CODE_LIST:
      case ContextModelTypeConst.APP_COUNTER:
      case ContextModelTypeConst.APP_ENTITIES:
      case ContextModelTypeConst.PAGES:
      case ContextModelTypeConst.CTRLS:
      case ContextModelTypeConst.UI_LOGIC:
      case ContextModelTypeConst.APP_PLUGIN:
      case ContextModelTypeConst.SYS_CSS:
      case ContextModelTypeConst.EDITOR_STYLE:
      case ContextModelTypeConst.VIEW_STYLE:
        return true;
      default:
        return false;
    }
  }
}
