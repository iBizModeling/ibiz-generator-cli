import { DSLHelper } from '@ibiz/rt-model-api';

export { plural, pluralLower } from './plural/plural';
export { ServicePathUtil } from './service-path-util/service-path-util';

export const dslHelper = new DSLHelper();
