export * from './constants';
export { TemplateModel } from './template-model';
export { TemplateModelData } from './template-model-data';

// 全局类型声明
declare global {
  interface IModel {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    [key: string | symbol]: any;
  }
}
