import { TemplateModel } from '@ibizlab/template-model';
import {
  TemplateFile,
  generator as g,
  macro,
  compiler,
  TemplateOutFile,
  manager,
  TemplateContextItem,
} from '@ibizlab/template-generator';
import { HandlebarsEngine } from '@ibizlab/template-handlebars';
import { join, normalize } from 'path';
import { notNilEmpty } from 'qx-util';
import { winToUnixPath } from '@ibizlab/template-core';
import { TemplateOptions } from './interface';
import { ModelWatcher, TemplateWatcher } from './util';

/**
 * iBizLab 模板引擎
 *
 * @author chitanda
 * @date 2021-12-23 13:12:59
 * @export
 * @class IBizLabTemplate
 */
export class IBizLabTemplate {
  readonly engine = new HandlebarsEngine();

  protected model!: TemplateModel;

  init(opts: TemplateOptions): void {
    this.model = new TemplateModel(opts.modelFolder, opts.mode, opts.app);
    g.setOptions({
      extension: this.engine.extension,
      macroFolderName: this.engine.macroFolderName,
      ...opts,
    });
    if (opts.dev) {
      this.devWatch(opts);
    }
    this.taps();
  }

  /**
   * 注册钩子
   *
   * @author chitanda
   * @date 2021-12-22 19:12:07
   * @protected
   */
  protected taps(): void {
    const tag = 'HandlebarsEngine';
    // 注册模板宏编译
    macro.hooks.complete.tap(tag, this.registerPartials.bind(this));
    // 编译器编译模板
    compiler.hooks.compile.tap(tag, this.parseTemplate.bind(this));
    // 计算模板的上下文
    manager.hooks.calcTempContext.tap(tag, this.calcTemplateContext.bind(this));
    // 模板文件读取完成
    manager.hooks.templateRead.tap(tag, this.handleTemplateFile.bind(this));
  }

  /**
   * 安装宏
   *
   * @author chitanda
   * @date 2021-12-21 14:12:27
   * @protected
   */
  protected registerPartials(macros: TemplateFile[]): void {
    macros.forEach(file => {
      this.engine.registerMacro(file.path, file.content as string);
    });
  }

  /**
   * 计算单个模板所需的上下文
   *
   * @author chitanda
   * @date 2021-12-24 13:12:59
   * @protected
   * @param {TemplateContextItem[]} ctxs
   * @param {TemplateFile} temp
   * @return {*}  {TemplateContextItem[]}
   */
  protected calcTemplateContext(
    ctxs: TemplateContextItem[],
    temp: TemplateFile,
  ): TemplateContextItem[] {
    try {
      const path = join(temp.folder, temp.name);
      const arr = this.engine.parseContextTags(winToUnixPath(path));
      const ctxMap = new Map<string, TemplateContextItem>();
      arr.forEach(tag => {
        const ctx = new TemplateContextItem(tag);
        ctxMap.set(ctx.name, ctx);
      });
      ctxMap.forEach(ctx => {
        ctxs.push(ctx);
      });
    } catch (err) {
      throw new Error(`模板: ${temp.path}, 异常: ${err.message}`);
    }
    return ctxs;
  }

  /**
   * 处理模板文件
   *
   * @author chitanda
   * @date 2021-12-24 14:12:39
   * @protected
   * @param {TemplateFile} file
   * @return {*}  {TemplateFile}
   */
  protected handleTemplateFile(file: TemplateFile): TemplateFile {
    if (file.isTemplate) {
      file.content = this.engine.preparse(file.content as string);
    }
    return file;
  }

  /**
   * 开发模式监控
   *
   * @author chitanda
   * @date 2022-08-08 19:08:03
   * @protected
   * @param {TemplateOptions} opts
   */
  protected devWatch(opts: TemplateOptions): void {
    const modelWatcher = new ModelWatcher(opts.modelFolder, fullPath => {
      this.model.update(fullPath);
    });
    const tempWatcher = new TemplateWatcher(opts.tempFolder);
    process.on('exit', () => {
      modelWatcher.close();
      tempWatcher.close();
    });
  }

  /**
   * 执行模板编译
   *
   * @author chitanda
   * @date 2021-12-19 14:12:37
   * @return {*}  {Promise<void>}
   */
  run(): Promise<void> {
    return g.execute();
  }

  /**
   * 编译模板文件
   *
   * @author chitanda
   * @date 2021-12-21 09:12:07
   * @protected
   * @param {TemplateFile} file
   * @return {*}  {void}
   */
  protected parseTemplate(file: TemplateFile): void {
    try {
      const models = this.model.getModelData(file);
      for (let i = 0; i < models.length; i++) {
        const item = models[i];
        const model = item.getModel();
        // 编译模板输出路径并替换 . 为系统路径分隔符
        const outFolder = normalize(
          this.engine.render(winToUnixPath(file.folder), model),
        );
        // 编译文件名称
        const outFileName = this.engine.render(file.name, model);
        if (file.isTemplate) {
          // 编译模板文件内容
          const content = this.engine.parse(
            file.content as HandlebarsTemplateDelegate,
            model,
          );
          if (notNilEmpty(content.trim())) {
            const out = new TemplateOutFile(outFolder, outFileName, content);
            compiler.pub(out);
          }
        } else {
          // 编译模板文件夹
          const out = new TemplateOutFile(
            outFolder,
            `${outFileName}${
              notNilEmpty(file.extension) ? `.${file.extension}` : ''
            }`,
            file.content as string,
          );
          compiler.pub(out);
        }
      }
    } catch (err) {
      console.error(err);
      throw new Error(`模板: ${file.path}, 异常: ${err.message}`);
    }
  }
}
// 唯一实例
export const template = new IBizLabTemplate();
