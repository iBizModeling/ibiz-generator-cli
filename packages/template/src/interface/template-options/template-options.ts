import { Options } from '@ibizlab/template-core';

/**
 * 模板参数
 *
 * @author chitanda
 * @date 2021-12-23 14:12:29
 * @export
 * @interface TemplateOptions
 * @extends {Options}
 */
// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface TemplateOptions extends Options {}
