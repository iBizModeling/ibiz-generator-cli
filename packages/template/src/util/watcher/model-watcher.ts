import { logger } from '@ibizlab/template-core';
import * as chokidar from 'chokidar';
import * as fs from 'fs-extra';

/**
 * 模型变更监控
 *
 * @author chitanda
 * @date 2022-08-08 17:08:01
 * @export
 * @class ModelWatcher
 */
export class ModelWatcher {
  protected w: chokidar.FSWatcher;

  /**
   * 路径缓存，避免第一次就触发。只有触发过再次变更的文件才认为变更
   *
   * @author chitanda
   * @date 2022-08-08 20:08:25
   * @protected
   * @type {Map<string, null>}
   */
  protected cache: Map<string, null> = new Map();

  /**
   * Creates an instance of ModelWatcher.
   *
   * @author chitanda
   * @date 2022-08-08 17:08:37
   * @param {string} modelFolder 监控的模板目录
   */
  constructor(
    protected modelFolder: string,
    protected modelChange: (fullPath: string) => void,
  ) {
    this.w = chokidar.watch(modelFolder);
    this.init();
  }

  /**
   * 初始化
   *
   * @author chitanda
   * @date 2022-05-24 14:05:34
   * @protected
   */
  protected init(): void {
    this.watchAddOrChange = this.watchAddOrChange.bind(this);
    this.watchErr = this.watchErr.bind(this);
    this.w.on('add', this.watchAddOrChange);
    this.w.on('change', this.watchAddOrChange);
    this.w.on('error', this.watchErr);
  }

  /**
   * 文件监控变更
   *
   * @author chitanda
   * @date 2022-05-24 14:05:18
   * @protected
   * @param {('add' | 'change')} eventName 事件类型
   * @param {string} fullPath 文件在操作系统中的路径
   * @param {(fs.Stats)} [stats] 文件信息
   */
  protected async watchAddOrChange(
    fullPath: string,
    stats?: fs.Stats,
  ): Promise<void> {
    if (stats) {
      if (stats.size <= 0) {
        return;
      }
    }
    if (!this.cache.has(fullPath)) {
      this.cache.set(fullPath, null);
      return;
    }
    this.modelChange(fullPath);
  }

  /**
   * 文件监控异常
   *
   * @author chitanda
   * @date 2022-05-24 14:05:13
   * @protected
   * @param {string} pathStr
   * @param {(fs.Stats | undefined)} [stats]
   */
  protected watchErr(pathStr: string, _stats?: fs.Stats | undefined): void {
    logger.debug('', `监控文件发生错误: ${pathStr}`);
  }

  /**
   * 停止监控
   *
   * @author chitanda
   * @date 2022-05-24 14:05:06
   */
  close() {
    this.w.off('add', this.watchAddOrChange);
    this.w.off('change', this.watchAddOrChange);
    this.w.off('error', this.watchErr);
    this.w.unwatch(this.modelFolder);
    this.w.close();
  }
}
