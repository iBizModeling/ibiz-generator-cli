import { winToUnixPath } from '@ibizlab/template-core';
import {
  macro,
  TemplateContextItem,
  TemplateFile,
} from '@ibizlab/template-generator';
import { HandlebarsEngine } from '@ibizlab/template-handlebars';
import { TemplateModel } from '@ibizlab/template-model';
import { readFileSync } from 'fs-extra';
import { basename, join, normalize } from 'path';
import { notNilEmpty } from 'qx-util';

/**
 * 模板预览
 *
 * @author chitanda
 * @date 2022-01-17 17:01:33
 * @export
 * @class TemplatePreview
 */
export class TemplatePreview {
  protected engine = new HandlebarsEngine();

  protected model: TemplateModel;

  /**
   * 模板后缀
   *
   * @author chitanda
   * @date 2022-01-18 18:01:08
   * @protected
   */
  protected extension = 'hbs';

  /**
   * Creates an instance of TemplatePreview.
   *
   * @author chitanda
   * @date 2022-01-17 17:01:48
   * @param {string} modelFolder 模型目录
   * @param {string} templateFolder 模板目录
   */
  constructor(public modelFolder: string, public templateFolder: string) {
    this.model = new TemplateModel(modelFolder);
    macro.setOptions({
      tempFolder: templateFolder,
      macroFolderName: '@macro',
      extension: this.extension,
    });
    const tag = 'TemplatePreview';
    macro.hooks.complete.tap(tag, this.registerPartials.bind(this));
  }

  /**
   * 安装宏
   *
   * @author chitanda
   * @date 2021-12-21 14:12:27
   * @protected
   */
  protected registerPartials(macros: TemplateFile[]): void {
    macros.forEach(file => {
      this.engine.registerMacro(file.path, file.content as string);
    });
  }

  /**
   * 计算单个模板所需的上下文
   *
   * @author chitanda
   * @date 2021-12-24 13:12:59
   * @protected
   * @param {TemplateContextItem[]} ctxs
   * @param {TemplateFile} temp
   * @return {*}  {TemplateContextItem[]}
   */
  protected calcTemplateContext(
    ctxs: TemplateContextItem[],
    temp: TemplateFile,
  ): TemplateContextItem[] {
    try {
      const path = join(temp.folder, temp.name);
      const arr = this.engine.parseContextTags(winToUnixPath(path));
      const ctxMap = new Map<string, TemplateContextItem>();
      arr.forEach(tag => {
        const ctx = new TemplateContextItem(tag);
        ctxMap.set(ctx.name, ctx);
      });
      ctxMap.forEach(ctx => {
        ctxs.push(ctx);
      });
    } catch (err) {
      throw new Error(`模板: ${temp.path}, 异常: ${err.message}`);
    }
    return ctxs;
  }

  protected createTemplateFile(path: string): TemplateFile {
    const fullPath = join(this.templateFolder, path);
    const fileName = basename(path);
    const fileContent = readFileSync(fullPath, 'utf-8');
    const tempFile = new TemplateFile(
      this.extension,
      path,
      fullPath,
      fileName,
      fileContent,
    );
    const ctxs: TemplateContextItem[] = this.calcTemplateContext([], tempFile);
    tempFile.contexts = ctxs;
    return tempFile;
  }

  /**
   * 编译指定模板
   *
   * @author chitanda
   * @date 2022-01-18 11:01:53
   * @param {string} templateFile 模板文件相对于模板文件夹的路径
   * @param {string} [modelId] 需要预览的模板，主模型对象标识。预览视图指定视图模型标识，预览实体指定实体模型标识。未指定默认取第一个该类型模型
   * @return {*}  {({ path: string; content: string } | null)}
   */
  execute(
    templateFile: string,
    modelId?: string,
  ): { path: string; content: string } | null {
    macro.syncScan();
    const file = this.createTemplateFile(templateFile);
    const models = this.model.getModelData(file);
    const model = models.find(m => {
      if (!modelId) {
        return true;
      }
      const data = m.getModel();
      const rootData = data[data.pubType as string] as IModel;
      if (rootData) {
        return rootData.id === modelId;
      }
    });
    if (model) {
      const data = model.getModel();
      // 编译模板文件内容
      try {
        const content = this.engine.parse(file.content as string, data);
        if (notNilEmpty(content)) {
          // 编译模板输出路径并替换 . 为系统路径分隔符
          const outFolder = normalize(
            this.engine.render(winToUnixPath(file.folder), data),
          );
          // 编译文件名称
          const outFileName = this.engine.render(file.name, data);
          if (process.send) {
            process.send(`${outFolder}、${outFileName}`);
          }
          return { path: join(outFolder, outFileName), content };
        }
      } catch (err) {
        throw new Error(`模板: ${file.path}, 异常: ${err.message}`);
      }
    } else if (modelId) {
      throw new Error(`未找到对应模板: ${file.path} 的指定模型: ${modelId}`);
    } else {
      throw new Error(`未找到对应模板: ${file.path} 的模型`);
    }
    return null;
  }

  /**
   * 根据发布类型编译模板
   *
   * @description {type} 具体类型查看 {@link ContextModelTypeConst}
   * @author chitanda
   * @date 2022-01-18 10:01:51
   * @param {string} type 发布对象类型
   * @param {(string | null)} subType 发布对象子类型
   * @param {string} content 模板内容
   * @param {string} [modelId] 需要预览的模板，主模型对象标识。预览视图指定视图模型标识，预览实体指定实体模型标识。未指定默认取第一个该类型模型
   * @return {*}  {(string | null)}
   */
  parse(
    type: string,
    subType: string | null,
    content: string,
    modelId?: string,
  ): string {
    const models = this.model.getModelDataByType(type, subType);
    const model = models.find(m => {
      if (!modelId) {
        return true;
      }
      const data = m.getModel();
      const rootData = data[data.pubType as string] as IModel;
      if (rootData) {
        return rootData.id === modelId;
      }
    });
    if (model) {
      return this.engine.render(content, model.getModel());
    }
    throw new Error('未找到指定模型');
  }
}
